package multiversum.multiversum;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class BateriaSetupTab extends android.support.v4.app.Fragment {

    NumberPicker _rowsNumberPicker = null;
    NumberPicker _colomnsNumberPicker = null;
    TextView _rowsTextView = null;
    TextView _colomnsTextView = null;
    Button _selectAllButton = null;
    Button _clearSelectionButton = null;
    TextView _sectionTextView = null;
    Spinner _sectionSpinner = null;
    boolean _batariaCreated = false;
    boolean _oneClientToLearnSelected = false;
    boolean _justCreatedView = false;
    boolean _ui_updateRunnableActive = false;
    boolean _updateUI_inLoop = false;
    Switch _setupModeSwitch = null;
    boolean _setupMode = true;
    int _numberOfClientsOnlineAndOffline = -1;

    //ArrayList< ArrayList < ImageView > > _allClientImages = new ArrayList<>();
    ArrayList< ArrayList <BateriaImageView> > _allClientImages = new ArrayList<>();

    Button _createBateriaButton = null;
    RelativeLayout _contentRelativeLayout = null;
    RelativeLayout _headerRelativeLayout = null;
    LinearLayout _footerRelativeLayout = null;
    int _contentHeight = -1;
    int _fragmentWidth = -1;
    int _calculatedImageSize = -1;
    boolean _serverRunning_old = false;
    boolean _serverRunning = false;
    Toast _currentToast = null;

    Switch _setByChipIDSwitch = null;
    boolean _setByIDMode = false;
    Button _blinkButton = null;
    Button _setButton = null;
    Spinner _chipIDSpinner = null;
    String _setByID_currentSelection = null;
    Position _setByID_markedPosition = null;

    Switch _toggleEffectsSwitch = null;
    boolean _toggeEffectsMode = false;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    Handler _handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            //Log.d( "_debug", "entered BateriaSetupTab::handleMessage()" );
            super.handleMessage(msg);
            updateUI();

            updateDataFromServer();

            //Log.d( "_debug", "leaving BateriaSetupTab::handleMessage()" );
        };
    };

    public void updateDataFromServer(){
        _serverRunning_old = _serverRunning;
        _serverRunning = getCppServerRunning();
    }

    @Override
    public void onAttach(Context context){
        //Log.d("_debug", "entering BateriaSetupTab::onAttach()");
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        //Log.d("_debug", "entering BateriaSetupTab::onCreate()");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        //Log.d("_debug", "entering BateriaSetupTab::onStart()");
        super.onStart();
        _updateUI_inLoop = true;
        start_updateUI_loop();
    }

    @Override
    public void onResume(){
        //Log.d("_debug", "entering BateriaSetupTab::onResume()");
        super.onResume();
        _handler.sendEmptyMessage(0);        //this is not absolutely necessary here, but it makes sure, that (after a swipe and back) the UI is in the old (running?) state immediately (without a small time where the gray "not-running" UI) can be seen
    }

    @Override
    public void onPause(){

        //Log.d("_debug", "entering BateriaSetupTab::onPause()");
        super.onPause();

    }

    @Override
    public void onStop() {

        //Log.d("_debug", "entering BateriaSetupTab::onStop()");
        super.onStop();
        _updateUI_inLoop = false;

        // well acutally, we don't need this any more, since the tabs don't get destroyed any more. (we called mViewPager.setOffscreenPageLimit(3) at MainActivity.java)
        /*
        //! we need to destroy the old imageViews here...
        for( int rowIndex = 0; rowIndex < _allClientImages.size(); rowIndex++ ){
            for( int colomnIndex = 0; colomnIndex < _allClientImages.get(rowIndex).size(); colomnIndex++ ){
                _contentRelativeLayout.removeView( _allClientImages.get(rowIndex).get(colomnIndex) );
            }
        }
        _allClientImages.clear();
        */

    }

    @Override
    public void onDestroy() {

        //Log.d("_debug", "entering BateriaSetupTab::onDestroy()");
        super.onDestroy();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        super.onCreateView(inflater, container, savedInstanceState);
        final View output = inflater.inflate( R.layout.bateria_setup_tab, container, false );

        _contentRelativeLayout = (RelativeLayout) output.findViewById( R.id.bateriaSetup_content_relativeLayout );
        _headerRelativeLayout = (RelativeLayout) output.findViewById( R.id.header );
        _footerRelativeLayout = (LinearLayout) output.findViewById( R.id.footer );

        _justCreatedView = true;

        initSetByIDStuff( output );

        initToggleEffectsStuff( output );

        initSectionSpinner( output );

        initSelectionButtons( output );

        initModeSwitch( output );

        initNumberPickerAndCreateButton(output);    // we need to initiate it here, even if we set it invisible in createBateriaGrind() but without that, the picker and buttons will not be associated with anything (or the wrong thing from an old swip to this tab)

        if( _batariaCreated ){
            int rows = getRowsOfBateria();
            int colomns = getColomnsOfBateria();
            createBateriaGrid( rows, colomns );
        }

        return output;
    }

    private void createBateriaGrid( int in_rows, int in_colomns ){

        Log.d("_debug", "entering BateriaSetupTab::createBateriaGrid()");
        _allClientImages = new ArrayList<>();
        for( int rowIndex = 0; rowIndex < in_rows; rowIndex++ ){
            ArrayList<BateriaImageView>  temp_row = new ArrayList<>();
            for( int colomnIndex = 0; colomnIndex < in_colomns; colomnIndex++ ){
                ImageView tempImageView = new ImageView( getActivity() );
                final BateriaImageView tempBateriaImageView = new BateriaImageView( tempImageView );
                temp_row.add( tempBateriaImageView );
            }
            _allClientImages.add( temp_row );
        }

        if( _contentHeight == -1 ){
            _contentHeight = _contentRelativeLayout.getHeight();
        }
        if( _fragmentWidth == -1 ){
            _fragmentWidth = _contentRelativeLayout.getWidth();
        }

        int defaultSize = (int)( ( (float) _contentHeight /2 ) + 0.5 );

        int potential_newSize_x = (int)(  ( (float)defaultSize ) / (float)in_colomns );
        int potential_newSize_y = (int)(  ( (float)defaultSize ) / (float)in_rows );
        int newSize = -1;
        if( potential_newSize_x < potential_newSize_y ){
            newSize = potential_newSize_x;
        }else{
            newSize = potential_newSize_y;
        }

        _rowsNumberPicker.setVisibility(View.GONE);
        _colomnsNumberPicker.setVisibility(View.GONE);
        _createBateriaButton.setVisibility(View.GONE);

        _rowsTextView.setVisibility(View.GONE);         // for some reason i just can't really remove the text-views...
        _colomnsTextView.setVisibility(View.GONE);

        _calculatedImageSize = newSize;
        setImageViews( in_rows, in_colomns );
        Log.d("_debug", "leaving BateriaSetupTab::createBateriaGrid()");

    }

    private void setImageViews( int in_rows, int in_colomns ){

        int x_delta_between_images = (int)( ( _fragmentWidth / (in_colomns + 1) ) + 0.5 );
        int y_delta_between_images = (int)( ( _contentHeight / (in_rows + 1) ) + 0.5 );

        Log.d("_debug", "entering BateriaSetupTab::setImageViews()");
        Bitmap nullInstrument = getBitmapForInstrumentString( "null" );  // because we need that a couple of times... that's speeding up the creating-process a lot
        for( int rowIndex = 0; rowIndex < in_rows; rowIndex++ ){
            int positionY = (int)( (float)( (y_delta_between_images*(rowIndex))-_calculatedImageSize/2 ) + 0.5 );       // i have no idea, why i calculate it exactly in this way, but it works more or less...
            for( int colomnIndex = 0; colomnIndex < in_colomns; colomnIndex++ ){

                String instrument = getInstrumentOnPosition(rowIndex, colomnIndex);
                Bitmap temp_bitMap;
                if( instrument.equals( "null" ) ){
                    temp_bitMap = nullInstrument;
                }else {
                    temp_bitMap = getBitmapForInstrumentString(instrument);
                }

                int positionX = (int)( (float)( (x_delta_between_images * (colomnIndex)) - _calculatedImageSize / 2 ) + 0.5*x_delta_between_images + 0.5 ); // i have no idea, why i calculate it exactly in this way, but it works more or less...
                Log.d("_debug", "positionX: " + positionX);

                RelativeLayout.LayoutParams temp_image_params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                temp_image_params.leftMargin = positionX;
                temp_image_params.topMargin = positionY;

                final int rowFinal = rowIndex;
                final int colomnFinal = colomnIndex;

                //_allClientImages.get(rowIndex).get(colomnIndex)._imageView.setImageBitmap( temp_bitMap );
                _allClientImages.get(rowIndex).get(colomnIndex).setBitmap( temp_bitMap );
                placeAndConfigureSingleImageView( rowFinal, colomnFinal, temp_image_params );

            }
        }
        Log.d("_debug", "leaving BateriaSetupTab::setImageViews()");
    }

    private void placeAndConfigureSingleImageView( final int in_row, final int in_colomn, RelativeLayout.LayoutParams in_imageParams ){

        _allClientImages.get(in_row).get(in_colomn)._imageView.setOnClickListener( new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if( _setupMode ){
                    int rotation = (int) _allClientImages.get(in_row).get(in_colomn)._imageView.getRotation();
                    if( rotation == 0 && _oneClientToLearnSelected == false ){
                        _allClientImages.get(in_row).get(in_colomn)._imageView.animate().rotation(45).start();
                        if( _setByIDMode == false ){
                            learnPosition(in_row, in_colomn);
                        }else{
                            _setByID_markedPosition.row = in_row;
                            _setByID_markedPosition.colomn = in_colomn;
                        }
                        _oneClientToLearnSelected = true;
                    }else if( rotation == 45 ){
                        _allClientImages.get(in_row).get(in_colomn)._imageView.animate().rotation(0).start();
                        clearLearnPosition();
                        _oneClientToLearnSelected = false;
                    }
                }else{
                    int rotation = (int) _allClientImages.get(in_row).get(in_colomn)._imageView.getRotation();
                    if( _toggeEffectsMode == false ){
                        if( rotation == 0 ){
                            _allClientImages.get(in_row).get(in_colomn)._imageView.animate().rotation(45).start();
                            addToIsMarkedForActions(in_row, in_colomn);
                        }else if( rotation == 45 ){
                            _allClientImages.get(in_row).get(in_colomn)._imageView.animate().rotation(0).start();
                            removeFromIsMarkedForActions(in_row, in_colomn);
                            _oneClientToLearnSelected = false;
                        }
                    }else{
                        toggleEffectOnPosition( in_row, in_colomn );
                    }
                }
            }
        });

        _allClientImages.get(in_row).get(in_colomn)._imageView.setOnLongClickListener(new View.OnLongClickListener(){
            @Override
            public boolean onLongClick(View v) {
                boolean output = false;
                if( _setupMode ){
                    startDialogToSelectBetweenSetInstrumentAndSetEffect( in_row, in_colomn );
                    output = true;    // we care about your long-click-belongings ;-)
                }else{
                    // here the stuff for long-clicking in non-setup-mode
                    // if you want to do something here...
                    // don't forget this:
                    // output = true;
                }
                return output;
            }
        });

        if( _setupMode == false ){
            boolean isMarked = getIsMarked( in_row, in_colomn );
            if( isMarked ){
                _allClientImages.get(in_row).get(in_colomn)._imageView.animate().rotation(45);
                //_allClientImages.get(in_row).get(in_colomn).setRotation((float)45.0);      // this is strange...
            }
        }
        _contentRelativeLayout.addView( _allClientImages.get(in_row).get(in_colomn)._imageView, in_imageParams );
    }

    private void toggleEffectOnPosition( int in_row, int in_colomn ){
        if( getHasEffect(in_row, in_colomn) == true ){
            boolean hasEffectOnPositionWhichIsCurrentlyOn = getHasActiveEffect(in_row, in_colomn);
            if( hasEffectOnPositionWhichIsCurrentlyOn ){
                boolean success = setEffectOnPositionToState( in_row, in_colomn, false );
                if( _currentToast != null ){
                    _currentToast.cancel();
                }
                if( success ){
                    _currentToast = Toast.makeText(getActivity(), "effect turned off", Toast.LENGTH_LONG);
                }else{
                    _currentToast = Toast.makeText(getActivity(), "failed to turn off effect", Toast.LENGTH_LONG);
                }
                _currentToast.show();
            }else{
                boolean success = setEffectOnPositionToState( in_row, in_colomn, true );
                if( _currentToast != null ){
                    _currentToast.cancel();
                }
                if( success ){
                    _currentToast = Toast.makeText(getActivity(), "effect turned on", Toast.LENGTH_LONG);
                }else{
                    _currentToast = Toast.makeText(getActivity(), "failed to turn on effect", Toast.LENGTH_LONG);
                }
                _currentToast.show();
            }
        }else{
            if( _currentToast != null ){
                _currentToast.cancel();
            }
            _currentToast = Toast.makeText( getActivity(), "no effect set on this position", Toast.LENGTH_LONG);
            _currentToast.show();
        }
    }

    private void startDialogToSelectBetweenSetInstrumentAndSetEffect(final int in_row, final int in_colomn ){

        boolean hasClientSet = getHasClientSetOnPosition( in_row, in_colomn );
        if( hasClientSet ){
            final String[] possibleOptions;
            boolean hasEffect = getHasEffect(in_row, in_colomn);
            if (hasEffect) {
                possibleOptions = new String[]{"set Instrument", "set (new) effect on this client", "edit current Effect"};
            } else {
                possibleOptions = new String[]{"set Instrument", "set effect on this client",};
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("wut do?");
            builder.setItems(possibleOptions, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int in_clickedOnItem) {
                    if (in_clickedOnItem == 0) {
                        startInstrumentAssociation(in_row, in_colomn);
                    } else if (in_clickedOnItem == 1) {
                        setEffectOnThisClient(in_row, in_colomn);
                    } else if (in_clickedOnItem == 2) {
                        editEffectOnThisClient(in_row, in_colomn);
                    } else {
                        Log.d("_debug", "BateriaSetupTab::startDialogToSelectBetweenSetInstrumentAndSetEffect(): strange behaviour");
                    }
                }
            });
            builder.show();
        }else{
            if(_currentToast != null){
                _currentToast.cancel();
            }
            Log.d( "_debug", "BateriaSetupTab::startDialogToSelectBetweenSetInstrumentAndSetEffect(): no client set on this position... do that first.." );
            _currentToast = Toast.makeText( getActivity(), "no client set on this position... do that first..", Toast.LENGTH_SHORT );
            _currentToast.show();
        }
    }

    private void setEffectOnThisClient(final int in_row, final int in_colomn ){

        final String[] possibleOptions = new String[]{"waterDrop", "immediately change of Color",};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("select effect?");
        builder.setItems(possibleOptions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int in_clickedOnItem) {

                if( in_clickedOnItem == 0 ){
                    Intent waterDropSetupIntend = new Intent( getContext(), WaterDropEffectSetup.class );
                    waterDropSetupIntend.putExtra( "new", true );
                    waterDropSetupIntend.putExtra( "row", in_row );
                    waterDropSetupIntend.putExtra( "colomn", in_colomn );
                    getContext().startActivity( waterDropSetupIntend );
                }else if( in_clickedOnItem == 1 ){
                    if(_currentToast != null){
                        _currentToast.cancel();
                    }
                    _currentToast = Toast.makeText( getActivity(), "BateriaSetupTab::setEffectOnThisClient(): empty function", Toast.LENGTH_SHORT);
                    _currentToast.show();
                }else{
                    Log.d("_debug", "BateriaSetupTab::setEffectOnThisClient(): strange behaviour");
                }
            }
        });
        builder.show();
    }

    private void editEffectOnThisClient( int in_row, int in_colomn ){
        Intent waterDropSetupIntend = new Intent( getContext(), WaterDropEffectSetup.class );
        waterDropSetupIntend.putExtra( "new", false );  // false, because we don't want to make a new effect but want to edit the old one
        waterDropSetupIntend.putExtra( "row", in_row );
        waterDropSetupIntend.putExtra( "colomn", in_colomn );
        getContext().startActivity( waterDropSetupIntend );
    }

    private Bitmap getBitmapForInstrumentString(String in_instrument ){
        Bitmap output;
        if( in_instrument.equals( "unknown" ) ){
            output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.unknown);       // means: there is a client associated with this position, but it has no Instrument-information
        }else if( in_instrument.equals( "null" ) ){
            output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.null_pointer);  // means: there is no clent associated with this positin
        }else if( in_instrument.equals( "mestre" ) ){
            output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.mestre);
        }else if( in_instrument.equals( "caixa" ) ){
            output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.caixa);
        }else if( in_instrument.equals( "tamborim" ) ){
            output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.tamborim);
        }else if( in_instrument.equals( "repique" ) ){
            output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.repinique);
        }else if( in_instrument.equals( "chocalho" ) ){
            output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.chocalho);
        }else if( in_instrument.equals( "primera" ) ){
            output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.surdo_primera);
        }else if( in_instrument.equals( "segunda" ) ){
            output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.surdo_segunda);
        }else if( in_instrument.equals( "tetiera" ) ){
            output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.surdo_tetiera);
        }else if( in_instrument.equals( "agogo" ) ){
            output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.agogo);
        }else{
            Log.d( "_debug", "getInstrumentOnPosition() returned bullshit!" );
            output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.bullshit);      // means: the return-value doesn't match any other pattern.. -> bateria not initiated??
        }

        output = Bitmap.createScaledBitmap(output, _calculatedImageSize, _calculatedImageSize, true);        // from: https://stackoverflow.com/questions/11756231/bitmapfactory-decoderesource-and-drawable-folders

        return output;
    }

    private void initSetByIDStuff( View in_view ){
        _setByChipIDSwitch = (Switch) in_view.findViewById(R.id.setByChipID_switch);
        if(_batariaCreated){
            if( _setupMode == false ){
                _setByChipIDSwitch.setVisibility(View.GONE);
            }else{
                _setByChipIDSwitch.setVisibility(View.VISIBLE);
            }
        }else{
            _setByChipIDSwitch.setVisibility(View.GONE);
        }

        _setByChipIDSwitch.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged( CompoundButton buttonView, boolean in_isChecked ){
                _setByIDMode = in_isChecked;
                deselectAllClients();
                if( in_isChecked ){
                    _blinkButton.setVisibility( View.VISIBLE );
                    _setButton.setVisibility( View.VISIBLE );
                    _chipIDSpinner.setVisibility( View.VISIBLE );
                }else{
                    _blinkButton.setVisibility( View.GONE );
                    _setButton.setVisibility( View.GONE );
                    _chipIDSpinner.setVisibility( View.GONE );
                }
            }
        });
        _chipIDSpinner = (Spinner) in_view.findViewById(R.id.chipID_spinner);


        if(_batariaCreated){
            if( _setupMode == false ){
                _chipIDSpinner.setVisibility(View.GONE);
            }else{
                if( _setByIDMode ){
                    _chipIDSpinner.setVisibility(View.VISIBLE);
                }else{
                    _chipIDSpinner.setVisibility(View.GONE);
                }
            }
        }else{
            _chipIDSpinner.setVisibility(View.GONE);
        }

        /*
        if(_batariaCreated){
            if( _setupMode == false ){
                _chipIDSpinner.setVisibility(View.GONE);
            }else{
                _chipIDSpinner.setVisibility(View.VISIBLE);
            }
        }else{
            _setByChipIDSwitch.setVisibility(View.GONE);
        }
        */


        final ArrayList<String> chipIDArray = new ArrayList<String>();
        int numberOfClients = collectClientsInformation(); //returns the number of clients, online and offline.
        for( int index = 0; index < numberOfClients; index++ ){
            chipIDArray.add( getChipIDFromSnapshotOnPosition(index) );
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>( getActivity(), android.R.layout.simple_spinner_dropdown_item, chipIDArray );
        spinnerArrayAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        _chipIDSpinner.setAdapter( spinnerArrayAdapter );
        _chipIDSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id){
                _setByID_currentSelection = chipIDArray.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        _setByID_markedPosition = new Position(-1, -1);
        _blinkButton = (Button) in_view.findViewById(R.id.blink_button);
        if(_batariaCreated){
            if( _setupMode == false ){
                _blinkButton.setVisibility(View.GONE);
            }else{
                if( _setByIDMode ){
                    _blinkButton.setVisibility(View.VISIBLE);
                }else{
                    _blinkButton.setVisibility(View.GONE);
                }
            }
        }else{
            _blinkButton.setVisibility(View.GONE);
        }
        _blinkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( _setByID_currentSelection != null ){
                    makeClientWithIDblink( _setByID_currentSelection );
                }
            }
        });
        /*
        if( _setByIDMode ){
            _blinkButton.setVisibility(View.VISIBLE);
        }else{
            _blinkButton.setVisibility(View.GONE);
        }
        */
        _setButton = (Button) in_view.findViewById(R.id.setByID_set_button);
        if(_batariaCreated){
            if( _setupMode == false ){
                _setButton.setVisibility(View.GONE);
            }else{
                _setButton.setVisibility(View.VISIBLE);
            }
        }else{
            _setButton.setVisibility(View.GONE);
        }
        _setButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( _setByID_currentSelection != null ){
                    if( _setByID_markedPosition.row != -1 && _setByID_markedPosition.colomn != -1 ){
                        assignClientWithIDtoPosition( _setByID_currentSelection, _setByID_markedPosition.row, _setByID_markedPosition.colomn );
                        _allClientImages.get( _setByID_markedPosition.row ).get( _setByID_markedPosition.colomn )._imageView.animate().rotation(0).start();
                        _setByID_markedPosition.row = -1;
                        _setByID_markedPosition.colomn = -1;
                    }else{
                        if( _currentToast != null ){
                            _currentToast.cancel();
                        }
                        _currentToast = Toast.makeText( getActivity(), "nothing marked", Toast.LENGTH_LONG );
                        _currentToast.show();
                    }
                }
            }
        });
    }

    private void initToggleEffectsStuff( View in_view ){
        _toggleEffectsSwitch = (Switch) in_view.findViewById( R.id.toggleEffects_switch );
        if(_batariaCreated == false){
            _toggleEffectsSwitch.setVisibility(View.GONE);
        }else{
            _toggleEffectsSwitch.setVisibility(View.VISIBLE);
        }
        _toggleEffectsSwitch.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged( CompoundButton buttonView, boolean in_isChecked ){
                _toggeEffectsMode = in_isChecked;
                deselectAllClients();
            }
        });
    }

    private void initSectionSpinner( View in_view ){
        _sectionSpinner = in_view.findViewById(R.id.section_spinner);
        _sectionTextView = in_view.findViewById(R.id.section_hint_textView);
        if( _setupMode ){
            _sectionSpinner.setVisibility(View.GONE);
            _sectionTextView.setVisibility(View.GONE);
        }else{
            _sectionSpinner.setVisibility(View.VISIBLE);
            _sectionTextView.setVisibility(View.VISIBLE);
        }

        final String[] instrumentArray = new String[]{ "none", "unknown", "mestre", "caixa", "tamborim", "repique", "chocalho", "primera", "segunda", "tetiera", "agogo" };
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>( getActivity(), android.R.layout.simple_spinner_dropdown_item, instrumentArray); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        _sectionSpinner.setAdapter( spinnerArrayAdapter );
        _sectionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                deselectAllClients();
                String selection = instrumentArray[position];

                if( selection != "none" ){
                    markAllFromSelection(selection);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                int foobar = -1;
            }
        });
    }

    private void initSelectionButtons( View in_view ){
        _selectAllButton = in_view.findViewById(R.id.select_all_button);
        if( _setupMode ){
            _selectAllButton.setVisibility(View.GONE);
        }else{
            _selectAllButton.setVisibility(View.VISIBLE);
        }
        _selectAllButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if( _setupMode == false ){
                    //Log.d("_debug", "selectAll-button pressed!");
                    selectAllClients();
                }else{
                    //Log.d("_debug", "selectAll-button pressed but still in setupMode");
                }
            }
        });

        _clearSelectionButton = in_view.findViewById(R.id.clear_selection_button);
        if( _setupMode ){
            _clearSelectionButton.setVisibility(View.GONE);
        }else{
            _clearSelectionButton.setVisibility(View.VISIBLE);
        }
        _clearSelectionButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if( _setupMode == false ){
                    //Log.d("_debug", "clearSelection-button pressed!");
                    deselectAllClients();
                    _sectionSpinner.setSelection(0);
                }else{
                    //Log.d("_debug", "clearSelection-button pressed but still in setupMode");
                }
            }
        });
    }

    public void initModeSwitch( View in_view ){
        _setupModeSwitch = (Switch) in_view.findViewById(R.id.setupMode_switch);
        if( _setupModeSwitch == null ){
            Log.d("_debug", "wut the fork?");
        }else {
            _setupModeSwitch.setChecked(_setupMode);
            if( _batariaCreated == false ){
                _setupModeSwitch.setVisibility(View.GONE);
            }else{
                _setupModeSwitch.setVisibility(View.VISIBLE);
            }
            _setupModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    _setupMode = isChecked;
                    if( isChecked ){
                        _clearSelectionButton.setVisibility(View.GONE);
                        _selectAllButton.setVisibility(View.GONE);
                        _sectionSpinner.setVisibility(View.GONE);
                        _sectionTextView.setVisibility(View.GONE);
                        _toggleEffectsSwitch.setVisibility(View.GONE);
                        _setByChipIDSwitch.setVisibility(View.VISIBLE);
                        if( _setByIDMode ){
                            _chipIDSpinner.setVisibility(View.VISIBLE);
                            _setButton.setVisibility(View.VISIBLE);
                            _blinkButton.setVisibility(View.VISIBLE);
                        }else{
                            _chipIDSpinner.setVisibility(View.GONE);
                            _setButton.setVisibility(View.GONE);
                            _blinkButton.setVisibility(View.GONE);
                        }
                        deselectAllClients();
                    }else{
                        if(_batariaCreated){
                            _selectAllButton.setVisibility(View.VISIBLE);
                            _clearSelectionButton.setVisibility(View.VISIBLE);
                            _sectionTextView.setVisibility(View.VISIBLE);
                            _sectionSpinner.setVisibility(View.VISIBLE);
                            _toggleEffectsSwitch.setVisibility(View.VISIBLE);
                            _setByChipIDSwitch.setVisibility(View.GONE);
                            _chipIDSpinner.setVisibility(View.GONE);
                            _setButton.setVisibility(View.GONE);
                            _blinkButton.setVisibility(View.GONE);
                            deselectAllClients();
                        }else{
                            Log.d("_debug", "buttons stay invisible because bataria not crated yet!");
                        }
                    }
                }
            });
        }
    }

    private void initNumberPickerAndCreateButton( View in_view ){

        _rowsNumberPicker = (NumberPicker) in_view.findViewById( R.id.rows_numberPicker );
        _rowsNumberPicker.setMinValue(1);
        _rowsNumberPicker.setMaxValue(10);
        _colomnsNumberPicker = (NumberPicker) in_view.findViewById( R.id.colomns_numberPicker );
        _colomnsNumberPicker.setMinValue(1);
        _colomnsNumberPicker.setMaxValue(10);

        _rowsTextView = (TextView) in_view.findViewById(R.id.rows_textView);
        _colomnsTextView = (TextView) in_view.findViewById(R.id.colomns_textView);

        _createBateriaButton = (Button) in_view.findViewById( R.id.createBateria_button );
        _createBateriaButton.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View in_view){
                int rows = _rowsNumberPicker.getValue();
                int colomns = _colomnsNumberPicker.getValue();
                _batariaCreated = true;

                _setupModeSwitch.setVisibility(View.VISIBLE);
                _setByChipIDSwitch.setVisibility(View.VISIBLE);
                if( _setByIDMode ){
                    _blinkButton.setVisibility(View.VISIBLE);
                    _setButton.setVisibility(View.VISIBLE);
                    _chipIDSpinner.setVisibility(View.VISIBLE);
                }else{
                    _blinkButton.setVisibility(View.GONE);
                    _setButton.setVisibility(View.GONE);
                    _chipIDSpinner.setVisibility(View.GONE);
                }

                //Log.d("_debug", "calling initBateria()");
                initBateria( rows, colomns );    // makes trouble with emulator... not with phone...
                //Log.d("_debug", "done");
                Log.d("_debug", "calling createBateriaGrid");
                createBateriaGrid( rows, colomns );
                Log.d("_debug", "done");

                if( _currentToast != null ){
                    _currentToast.cancel();
                }
                _currentToast = Toast.makeText( getActivity(), "bateria created...", Toast.LENGTH_SHORT );
                _currentToast.show();

            }
        });
    }

    public void updateUI(){
        //Log.d( "_debug", "entering BateriaStatusTab::updateUI()" );

        if( (_serverRunning && _serverRunning_old == false) || (_justCreatedView && _serverRunning) ){
            _contentRelativeLayout.setBackgroundColor(Color.WHITE);
            _headerRelativeLayout.setBackgroundColor(Color.WHITE);
            _footerRelativeLayout.setBackgroundColor(Color.WHITE);
            _justCreatedView = false;
        }else if( _serverRunning == false && _serverRunning_old || (_justCreatedView && _serverRunning == false) ){
            _contentRelativeLayout.setBackgroundColor(Color.GRAY);
            _headerRelativeLayout.setBackgroundColor(Color.GRAY);
            _footerRelativeLayout.setBackgroundColor(Color.GRAY);
            _justCreatedView = false;
        }

        if( getSomeClientToInstrumentAssignmentHasChanged() ){
            for( int rowIndex = 0; rowIndex < _allClientImages.size(); rowIndex++ ){
                for( int colomnIndex = 0; colomnIndex < _allClientImages.get(rowIndex).size(); colomnIndex++ ){
                    String oldInstrumentStringOnPosition = String.valueOf( _allClientImages.get(rowIndex).get(colomnIndex)._imageView.getTag() );
                    String instrumentString = getInstrumentOnPosition( rowIndex, colomnIndex );
                    if( oldInstrumentStringOnPosition.equals(instrumentString) == false ){
                        if( oldInstrumentStringOnPosition.equals( "null" ) && instrumentString.equals( "unknown" ) ){   // usually the case after learning a new client...
                            startInstrumentAssociation( rowIndex, colomnIndex );
                            _allClientImages.get(rowIndex).get(colomnIndex)._imageView.animate().rotation(0).start();
                            removeFromIsMarkedForActions( rowIndex, colomnIndex );
                            _oneClientToLearnSelected = false;
                            blinkOnPositionWithColor( rowIndex, colomnIndex, 255, 255, 255, 0 );
                        }else{
                            Bitmap newBitmap = getBitmapForInstrumentString(instrumentString);
                            _allClientImages.get(rowIndex).get(colomnIndex).setBitmap(newBitmap);
                            _allClientImages.get(rowIndex).get(colomnIndex)._imageView.setTag( instrumentString );
                            blinkOnPositionWithColor( rowIndex, colomnIndex, 0, 0, 255, 0 );
                        }
                    }
                }
            }
        }

        int numberOfClients = collectClientsInformation(); //returns the number of clients, online and offline.
        if( _numberOfClientsOnlineAndOffline !=  numberOfClients ){
            _numberOfClientsOnlineAndOffline = numberOfClients;
            // is this the right place for this? maybe i should move it out of this if-statement. but this way safes performence
            final ArrayList<String> chipIDArray = new ArrayList<String>();

            for (int index = 0; index < numberOfClients; index++) {
                chipIDArray.add(getChipIDFromSnapshotOnPosition(index));
            }
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, chipIDArray);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            _chipIDSpinner.setAdapter(spinnerArrayAdapter);
            _chipIDSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    _setByID_currentSelection = chipIDArray.get(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        for( int rowIndex = 0; rowIndex < _allClientImages.size(); rowIndex++ ){
            for( int colomnIndex = 0; colomnIndex < _allClientImages.get( rowIndex ).size(); colomnIndex++ ){
                if( getHasEffect( rowIndex, colomnIndex ) ){
                    if( getHasActiveEffect( rowIndex, colomnIndex ) ){  // this will cause the bitmap to blink
                        if( _allClientImages.get(rowIndex).get(colomnIndex)._isInverted ){
                            _allClientImages.get(rowIndex).get(colomnIndex).switchToNormal();
                        }else{
                            _allClientImages.get(rowIndex).get(colomnIndex).switchToInverted();
                        }
                    }else{
                        if( _allClientImages.get(rowIndex).get(colomnIndex)._isInverted == false ){
                            _allClientImages.get(rowIndex).get(colomnIndex).switchToInverted();
                        }else{
                            _allClientImages.get(rowIndex).get(colomnIndex).switchToNormal();
                        }
                    }
                }
            }
        }
        //Log.d("_debug", "leaving BateriaStatusTab::updateUI()");
    }

    public  void start_updateUI_loop(){

        Runnable runnable = new Runnable(){
            @Override
            public void run(){

                while( _ui_updateRunnableActive ){  //to make absolutely sure, there is only one ui-update-thread
                    delay(300);
                    //Log.d("_debug", "BateriaSetupTab::start_updateUI_loop(): _ui_updateRunnableActive == true" );
                }

                _ui_updateRunnableActive = true;
                //delay(700 ); //this is just for a better feeling... we pretend the app must du a lot of stuff by introducing this delay here...
                updateDataFromServer();
                while( _updateUI_inLoop ){
                    //Log.d("_debug", "BateriaSetupTab::start_updateUI_loop(): calling _handler.sendEmptyMessage()");
                    _handler.sendEmptyMessage(0);
                    delay(1000 );   //wait 1 second
                }
                //Log.d("_debug", "BateriaSetupTab::start_updateUI_loop(): leaving ui-runnable!!!!!!!!!!!!!!!!!!!!!!!!!!");
                _ui_updateRunnableActive = false;
            }
        };
        if( _ui_updateRunnableActive == false ){    // after a veerry quick swipe to the non-neighbour-tab (e.g. Actions) and back it might be the case, that the runnable is still active (becuase it was sleeping). to avoid to start another one we check that here...
            //Log.d("_debug", "BateriaSetupTab: starting new ui-runnable.....................");
            Thread thread = new Thread(runnable);
            thread.start();
        }else{
            //Log.d("_debug", "BateriaSetupTab::start_updateUI_loop(): not starting a new runnable, because _ui_updateRunnableActive == true");
        }
    }

    private void delay( int in_delay ){
        synchronized (this) {   // why ever i need this...
            try {
                wait(in_delay);
            } catch (Exception exception) {
                Log.d("_debug", " ... wait(" + in_delay + ") throwed exception: " + exception);
            }
        }
    }

    private void selectAllClients(){
        for( int rowIndex = 0; rowIndex < _allClientImages.size(); rowIndex++ ){
            for( int colomnIndex = 0; colomnIndex < _allClientImages.get(rowIndex).size(); colomnIndex++ ){
                int rotation = (int) _allClientImages.get(rowIndex).get(colomnIndex)._imageView.getRotation();
                if(rotation == 0){
                    _allClientImages.get(rowIndex).get(colomnIndex)._imageView.animate().rotation(45).start();
                    _oneClientToLearnSelected = true;
                    addToIsMarkedForActions( rowIndex, colomnIndex );
                }
            }
        }
    }

    private void deselectAllClients(  ){

        for( int rowIndex = 0; rowIndex < _allClientImages.size(); rowIndex++ ){

            for( int colomnIndex = 0; colomnIndex < _allClientImages.get(rowIndex).size(); colomnIndex++ ){

                int rotation = (int) _allClientImages.get(rowIndex).get(colomnIndex)._imageView.getRotation();
                if(rotation == 45){
                    _allClientImages.get(rowIndex).get(colomnIndex)._imageView.animate().rotation(0).start();
                    _oneClientToLearnSelected = false;
                    removeFromIsMarkedForActions( rowIndex, colomnIndex );
                }

            }

        }
        //_sectionSpinner.setSelection(0);
    }

    private void markAllFromSelection(String in_instrument ){
        for( int rowIndex = 0; rowIndex < _allClientImages.size(); rowIndex++ ){
            for( int colomnIndex = 0; colomnIndex < _allClientImages.get(rowIndex).size(); colomnIndex++ ){
                String tagFromImageView = String.valueOf(_allClientImages.get(rowIndex).get(colomnIndex)._imageView.getTag());
                if( tagFromImageView.equals( in_instrument ) ) {
                    int rotation = (int) (_allClientImages.get(rowIndex).get(colomnIndex)._imageView.getRotation());
                    if (rotation == 0) {
                        _allClientImages.get(rowIndex).get(colomnIndex)._imageView.animate().rotation(45).start();
                        addToIsMarkedForActions( rowIndex, colomnIndex);
                    } else {
                        _allClientImages.get(rowIndex).get(colomnIndex)._imageView.animate().rotation(0).start();
                        removeFromIsMarkedForActions( rowIndex, colomnIndex );
                    }
                }
            }
        }
    }

    private void startInstrumentAssociation( final int in_row, final int in_colomn ){
        final String[] instrumentArray = new String[]{"unknown", "mestre", "caixa", "tamborim", "repique", "chocalho", "primera", "segunda", "tetiera", "agogo"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Pick an Instrument");
        builder.setItems(instrumentArray, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int in_clickedOnItem) {

                String instrumentString = "";
                if( instrumentArray[in_clickedOnItem].equals( "unknown" ) ){
                    _allClientImages.get(in_row).get(in_colomn)._imageView.setTag("unknown");
                    instrumentString = "unknown";
                }else if( instrumentArray[in_clickedOnItem].equals( "mestre" ) ){
                    _allClientImages.get(in_row).get(in_colomn)._imageView.setTag("mestre");
                    instrumentString = "mestre";
                }else if( instrumentArray[in_clickedOnItem].equals( "caixa" ) ){
                    _allClientImages.get(in_row).get(in_colomn)._imageView.setTag("caixa");
                    instrumentString = "caixa";
                }else if( instrumentArray[in_clickedOnItem].equals( "tamborim" ) ){
                    _allClientImages.get(in_row).get(in_colomn)._imageView.setTag("tamborim");
                    instrumentString = "tamborim";
                }else if( instrumentArray[in_clickedOnItem].equals( "repique" ) ){
                    _allClientImages.get(in_row).get(in_colomn)._imageView.setTag("repique");
                    instrumentString = "repique";
                }else if( instrumentArray[in_clickedOnItem].equals( "chocalho" ) ){
                    _allClientImages.get(in_row).get(in_colomn)._imageView.setTag("chocalho");
                    instrumentString = "chocalho";
                }else if( instrumentArray[in_clickedOnItem].equals( "primera" ) ){
                    _allClientImages.get(in_row).get(in_colomn)._imageView.setTag("primera");
                    instrumentString = "primera";
                }else if( instrumentArray[in_clickedOnItem].equals( "segunda" ) ){
                    _allClientImages.get(in_row).get(in_colomn)._imageView.setTag("segunda");
                    instrumentString = "segunda";
                }else if( instrumentArray[in_clickedOnItem].equals( "tetiera" ) ){
                    _allClientImages.get(in_row).get(in_colomn)._imageView.setTag("tetiera");
                    instrumentString = "tetiera";
                }else if(instrumentArray[in_clickedOnItem].equals( "agogo" ) ){
                    _allClientImages.get(in_row).get(in_colomn)._imageView.setTag("agogo");
                    instrumentString = "agogo";
                }else{
                    //Log.d("_debug", "unknown input...");
                    _allClientImages.get(in_row).get(in_colomn)._imageView.setTag("unknown input");
                    instrumentString = "unknown input";
                }
                boolean successfullySet = setInstrumentOnPosition(instrumentString, in_row, in_colomn);
                if( successfullySet ){
                    //_allClientImages.get(in_row).get(in_colomn)._imageView.setImageBitmap(getBitmapForInstrumentString(instrumentArray[in_clickedOnItem]));
                    _allClientImages.get(in_row).get(in_colomn).setBitmap(getBitmapForInstrumentString(instrumentArray[in_clickedOnItem]));
                }else{
                    Toast.makeText( getActivity(), "failed to set instrument... no client associated", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }

    public native void initBateria( int in_rows, int in_colomns );
    public native String getInstrumentOnPosition(int in_rows, int in_colomns);
    public native void learnPosition( int in_row, int in_colomn );
    public native void clearLearnPosition();
    public native boolean getCppServerRunning();
    public native boolean setInstrumentOnPosition( String in_instrument, int in_row, int in_colomn );
    public native void addToIsMarkedForActions( int in_row, int in_colomn );
    public native void removeFromIsMarkedForActions( int in_row, int in_colomn );
    public native int getRowsOfBateria();
    public native int getColomnsOfBateria();
    public native boolean getSomeClientToInstrumentAssignmentHasChanged();
    public native void blinkOnPositionWithColor( int in_row, int in_colomn, int in_red, int in_green, int in_blue, int in_uv );
    public native boolean getIsMarked( int in_row, int in_colomn );
    public native void makeClientWithIDblink( String in_chipID );
    public native void assignClientWithIDtoPosition( String in_chipID, int in_row, int in_colomn );
    public native int collectClientsInformation();
    public native String getChipIDFromSnapshotOnPosition( int in_positionInArray );
    public native boolean getHasActiveEffect( int in_row, int in_colomn );
    public native boolean getHasEffect( int in_row, int in_colomn );
    public native boolean getHasClientSetOnPosition( int in_row, int in_colomn );
    public native boolean setEffectOnPositionToState( int in_row, int in_colomn, boolean in_state );

}






