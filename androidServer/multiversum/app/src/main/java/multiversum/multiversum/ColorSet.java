package multiversum.multiversum;


public class ColorSet {

    public int red = 0;
    public int green = 0;
    public int blue = 0;
    public int uv = 0;

    public ColorSet(){
    }

    public ColorSet(int in_red, int in_green, int in_blue, int in_uv) {
        red = in_red;
        green = in_green;
        blue = in_blue;
        uv = in_uv;
    }
}
