package multiversum.multiversum;


import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

class CustomAdapterEffectsList extends ArrayAdapter{

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    public CustomAdapterEffectsList( @NonNull Context context, EffectsInformation[] input ) {
        super( context, R.layout.row_effects_list, input );
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable final View convertView, @NonNull ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        LayoutInflater someInflater = LayoutInflater.from( getContext() );
        final View customView = someInflater.inflate( R.layout.row_effects_list, parent, false );


        final EffectsInformation singleInformation = (EffectsInformation) getItem( position );
        TextView effectNameTextView = (TextView) customView.findViewById( R.id.effect_name_textView );
        effectNameTextView.setText(         "effect name: " + singleInformation._effectName );
        effectNameTextView.setTypeface(Typeface.MONOSPACE);

        ImageView effectImageView = (ImageView) customView.findViewById( R.id.effectsList_effect_imageView );
        effectImageView.setImageBitmap( singleInformation._effectPicture );

        TextView speedTextView = (TextView) customView.findViewById( R.id.speed_textView );
        speedTextView.setText(        "speed: " + singleInformation._speed );
        speedTextView.setTypeface( Typeface.MONOSPACE );


        TextView positionTextView = (TextView) customView.findViewById( R.id.positionOfClient_textView );
        positionTextView.setText( "on client(row,colomn): " + singleInformation._positionOfClientWithThisEffect.row + "," + singleInformation._positionOfClientWithThisEffect.colomn );
        positionTextView.setTypeface( Typeface.MONOSPACE );

        TextView statusTextView = (TextView) customView.findViewById( R.id.status_textView );
        statusTextView.setText(       "status:  " + singleInformation._active );
        statusTextView.setTypeface( Typeface.MONOSPACE );

        TextView chipID_TextView = (TextView) customView.findViewById( R.id.onChip_textView );
        chipID_TextView.setText(      "chipID:     " + singleInformation._chipIDofClientWithThisEffect );
        chipID_TextView.setTypeface( Typeface.MONOSPACE );


        final SeekBar triggerThresholdSeekBar = (SeekBar) customView.findViewById( R.id.triggerThreshold_seekBar );
        triggerThresholdSeekBar.setMax( 1023 );
        triggerThresholdSeekBar.setProgress( singleInformation._currentTriggerThreshold );
        triggerThresholdSeekBar.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch( SeekBar seekBar ){
                int newTriggerThreshold = seekBar.getProgress();    // ok like this? id did this in a different way in the clients-list-adapter. that worked...
                TextView triggerThresholdTextView = (TextView) customView.findViewById( R.id.trigger_threshold_textView );
                triggerThresholdTextView.setText( "threshold: " + newTriggerThreshold );
                setEffectTriggerThresholdOnClientOnPositionToValue( singleInformation._positionOfClientWithThisEffect.row, singleInformation._positionOfClientWithThisEffect.colomn, newTriggerThreshold);
            }
        });


        TextView triggerThresholdTextView = (TextView) customView.findViewById( R.id.trigger_threshold_textView );
        triggerThresholdTextView.setText( "threshold: " + singleInformation._currentTriggerThreshold );
        triggerThresholdTextView.setTypeface( Typeface.MONOSPACE );

        Button blinkButton = (Button) customView.findViewById( R.id.blink_button);
        blinkButton.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String chipID = singleInformation._chipIDofClientWithThisEffect;
                makeClientWithIDblink( chipID );
            }
        });

        Button triggerButton = (Button) customView.findViewById( R.id.trigger_button );
        triggerButton.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                manuallyTriggerEffectOnClientOnPosition( singleInformation._positionOfClientWithThisEffect.row, singleInformation._positionOfClientWithThisEffect.colomn );
            }
        });


        return customView;
    }

    public native void makeClientWithIDblink( String in_chipID );
    public native void setEffectTriggerThresholdOnClientOnPositionToValue( int in_row, int in_colomn, int in_newTriggerThreshold );
    public native void manuallyTriggerEffectOnClientOnPosition( int in_row, int in_colomn );

}
