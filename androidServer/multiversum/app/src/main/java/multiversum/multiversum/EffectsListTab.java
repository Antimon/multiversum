package multiversum.multiversum;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class EffectsListTab extends android.support.v4.app.Fragment {

    boolean _justCreatedView = false;
    boolean _ui_updateRunnableActive = false;
    boolean _updateUI_inLoop = false;

    boolean _serverRunning_old = false;
    boolean _serverRunning = false;

    ImageButton _refreshImageButton = null;

    ListView _effectsList = null;
    LinearLayout _linearLayout = null;

    TextView _numberOfEffectsTextView = null;

    //!View _thisView = null;

    Bitmap _unknownBitmap = null;
    Bitmap _waterDropBitmap = null;
    Bitmap _switchColorBitmap = null;
    Bitmap _bullshitBitmap = null;

    Handler _handler = new Handler(){
        @Override
        public void handleMessage( Message msg ) {
            //Log.d( "_debug", "entered BateriaSetupTab::handleMessage()" );
            super.handleMessage(msg);
            updateUI();

            updateDataFromServer();

            //Log.d( "_debug", "leaving BateriaSetupTab::handleMessage()" );
        };
    };

    public void updateDataFromServer(){
        _serverRunning_old = _serverRunning;
        _serverRunning = getCppServerRunning();
    }

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }


    @Override
    public void onAttach(Context context) {
        //Log.d("_debug", "entering OnMarkedClientsTab::onAttach()");
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        //Log.d("_debug", "entering OnMarkedClientsTab::onCreate()");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        //Log.d("_debug", "entering ActionTab::onStart()");
        super.onStart();
        _updateUI_inLoop = true;
        start_updateUI_loop();
    }

    @Override
    public void onResume() {
        //Log.d("_debug", "entering OnMarkedClientsTab::onResume()");
        super.onResume();
        _handler.sendEmptyMessage(0);        //this is not absolutely necessary here, but it makes sure, that (after a swipe and back) the UI is in the old (running?) state immediately (without a small time where the gray "not-running" UI) can be seen
    }

    @Override
    public void onPause() {
        //Log.d("_debug", "entering OnMarkedClientsTab::onPause()");
        super.onPause();
    }

    @Override
    public void onStop() {
        //Log.d("_debug", "entering OnMarkedClientsTab::onStop()");
        super.onStop();
        _updateUI_inLoop = false;
    }

    @Override
    public void onDestroy() {
        //Log.d("_debug", "entering OnMarkedClientsTab::onDestroy()");
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState ){
        super.onCreateView(inflater, container, savedInstanceState);
        View output = inflater.inflate( R.layout.effects_list_tab, container, false );

        _linearLayout = (LinearLayout) output.findViewById( R.id.effects_LinearLayout);
        _linearLayout.setBackgroundColor( Color.GRAY );

        _effectsList = (ListView) output.findViewById(R.id.effects_ListView);

        _justCreatedView = true;

        _unknownBitmap = (Bitmap) BitmapFactory.decodeResource( getContext().getResources(), R.drawable.unknown );
        _unknownBitmap = Bitmap.createScaledBitmap( _unknownBitmap, 100, 100, true );
        _waterDropBitmap = (Bitmap) BitmapFactory.decodeResource( getContext().getResources(), R.drawable.water_drop_fx );
        _waterDropBitmap = Bitmap.createScaledBitmap( _waterDropBitmap, 100, 100, true );
        _switchColorBitmap = (Bitmap) BitmapFactory.decodeResource( getContext().getResources(), R.drawable.switch_fx );
        _switchColorBitmap = Bitmap.createScaledBitmap( _switchColorBitmap, 100, 100, true );
        _bullshitBitmap = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.bullshit);
        _bullshitBitmap = Bitmap.createScaledBitmap(_bullshitBitmap, 100, 100, true);
        _refreshImageButton = output.findViewById( R.id.refresh_imageButton );
        _refreshImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buildList();
                Log.d("_debug", "test");
            }
        });
        buildList();
        _numberOfEffectsTextView = (TextView) output.findViewById( R.id.effects_numberOfEffects_TextView );
        updateNumberOfEffects( output );
        return output;
    }

    private void updateNumberOfEffects( View in_View ){
        int numberOfEffects = getTotalNumberOfEffects();
        if( numberOfEffects != -1 ){
            _numberOfEffectsTextView.setText("number of effects: " + numberOfEffects);
        }else{
            _numberOfEffectsTextView.setText("bateria not created...");
        }
    }

    private void buildList(){
        int numberOfEffects = collectEffectsInformation();
        if( numberOfEffects != -1 ){
            //TextView numberOfEffectsTextView = (TextView) _thisView.findViewById(R.id.effects_numberOfEffects_TextView);
            // _numberOfEffectsTextView.setText("number of effects: " + numberOfEffects);
            _numberOfEffectsTextView.setText("" + numberOfEffects);
            EffectsInformation[] listContent = new EffectsInformation[numberOfEffects];
            for( int index = 0; index < numberOfEffects; index++ ){
                String effectName = getEffectnameOfEffectFromSnapshotOnPosition(index);
                Bitmap effectPicture = getBitmapOfEffect(effectName);
                int redValue = getEffectColorRedFromSnapshotOnPosition(index);
                int greenValue = getEffectColorGreenFromSnapshotOnPosition(index);
                int blueValue = getEffectColorBlueFromSnapshotOnPosition(index);
                int uvValue = getEffectColorUvFromSnapshotOnPosition(index);
                boolean stayOnColor = getStayOnColorValueFromSnapshotOnPosition(index);
                int currentTriggerThreshold = getCurrentTriggerThresholdFromSnapshotOnPosition(index);
                int speed = getSpeedFromSnapshotOnPosition(index);
                int attack = getAttackFromSnapshotOnPosition(index);
                int release = getReleaseFromSnapshotOnPosition(index);
                boolean active = getStateOfEffectFromSnapshotOnPosition(index);
                int row = getRowFromSnapshotOnPosition(index);
                int colomn = getColomnFromSnapshotOnPosition(index);
                String chipID = getChipIDOfClientWithEffectFromSnapshotOnPosition(index);
                EffectsInformation temp_information = new EffectsInformation(effectName, redValue, greenValue, blueValue, uvValue, effectPicture, stayOnColor, currentTriggerThreshold, speed, attack, release, active, row, colomn, chipID);
                listContent[index] = temp_information;
            }

            ListAdapter effectsListAdapter = new CustomAdapterEffectsList( getActivity(), listContent );
            _effectsList.setAdapter( effectsListAdapter );  //! dit macht problems

            _effectsList.setOnItemClickListener( new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Toast toast = Toast.makeText(getActivity(), "clicked", Toast.LENGTH_SHORT);
                    toast.show();
                }
            });
        }
    }

    Bitmap getBitmapOfEffect( String in_instrument ){
        Bitmap output;
        if( in_instrument.equals( "unknown" ) ){
            output = _unknownBitmap;
        }else if( in_instrument.equals( "waterDrop" ) ){
            output = _waterDropBitmap;
        }else if( in_instrument.equals( "switchColor" ) ){
            output = _switchColorBitmap;
        }else{
            output = _bullshitBitmap;
        }
        return output;
    }

    private void delay( int in_delay ){
        synchronized (this) {   // why ever i need this...
            try {
                wait(in_delay);
            } catch (Exception exception) {
                Log.d("_debug", " ... wait(" + in_delay + ") throwed exception: " + exception);
            }
        }
    }

    public void start_updateUI_loop(){
        Runnable runnable = new Runnable(){
            @Override
            public void run(){
                while( _ui_updateRunnableActive ){  //to make absolutely sure, there is only one ui-update-thread
                    delay(300);
                }
                _ui_updateRunnableActive = true;
                updateDataFromServer();
                while( _updateUI_inLoop ){
                    _handler.sendEmptyMessage(0);
                    delay(1000 );   //wait 1 second
                }
                _ui_updateRunnableActive = false;
            }
        };
        if( _ui_updateRunnableActive == false ){    // after a veerry quick swipe to the non-neighbour-tab (e.g. Actions) and back it might be the case, that the runnable is still active (becuase it was sleeping). to avoid to start another one we check that here...
            Thread thread = new Thread(runnable);
            thread.start();
        }else{
            //Log.d("_debug", "BateriaSetupTab::start_updateUI_loop(): not starting a new runnable, because _ui_updateRunnableActive == true");
        }
    }

    public void updateUI(){
        //Log.d( "_debug", "entering BateriaStatusTab::updateUI()" );
        if( (_serverRunning && _serverRunning_old == false) || (_justCreatedView && _serverRunning) ){
            _linearLayout.setBackgroundColor(Color.WHITE);
            _justCreatedView = false;
        }else if( _serverRunning == false && _serverRunning_old || (_justCreatedView && _serverRunning == false) ){
            _linearLayout.setBackgroundColor(Color.GRAY);
            _justCreatedView = false;
        }
    }

    public native boolean getCppServerRunning();
    public native int getTotalNumberOfEffects();
    public native int collectEffectsInformation();  // returns the number of available effect-information-packs
    public native String getEffectnameOfEffectFromSnapshotOnPosition( int in_index );
    public native int getEffectColorRedFromSnapshotOnPosition( int in_index );
    public native int getEffectColorGreenFromSnapshotOnPosition( int in_index );
    public native int getEffectColorBlueFromSnapshotOnPosition( int in_index );
    public native int getEffectColorUvFromSnapshotOnPosition( int in_index );
    public native boolean getStayOnColorValueFromSnapshotOnPosition( int in_index );
    public native int getCurrentTriggerThresholdFromSnapshotOnPosition( int in_index );
    public native int getSpeedFromSnapshotOnPosition( int in_index );
    public native int getAttackFromSnapshotOnPosition( int in_index );
    public native int getReleaseFromSnapshotOnPosition( int in_index );
    public native boolean getStateOfEffectFromSnapshotOnPosition( int in_index );
    public native int getRowFromSnapshotOnPosition( int in_index );
    public native int getColomnFromSnapshotOnPosition( int in_index);
    public native String getChipIDOfClientWithEffectFromSnapshotOnPosition( int in_index );

}
