package multiversum.multiversum;


import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


public class MainActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    // Used to load the 'native-lib' library on application startup.

    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        //Log.d("_debug", "entering MainActivity::onCreate()");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        initServer( Environment.getExternalStorageDirectory().getPath() );

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById( R.id.container );
        mViewPager.setOffscreenPageLimit(5);    // https://stackoverflow.com/questions/8348707/prevent-viewpager-from-destroying-off-screen-views
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        // https://stackoverflow.com/questions/30616474/android-support-design-tablayout-gravity-center-and-mode-scrollable
        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        //Log.d("_debug", "leaving MainAcitivity::onCreate()");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        //getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        // if (id == R.id.action_settings) {
        //    return true;
        // }

        return super.onOptionsItemSelected(item);
    }


    //A placeholder fragment containing a simple view.
    public static class PlaceholderFragment extends Fragment {

        //The fragment argument representing the section number for thisfragment.
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        //Returns a new instance of this fragment for the given section number.
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            //View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            View rootView = inflater.inflate(R.layout.server_tab, container, false);
            //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            //textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;

        }
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter{
        public SectionsPagerAdapter(FragmentManager in_fm){
            super(in_fm);
        }

        @Override
        public Fragment getItem(int in_position) {
            //Fragment output = null;
            switch( in_position ){
                case 0:
                    ServerTab status_tab = new ServerTab();
                    return status_tab;
                case 1:
                    BateriaSetupTab bateria_tab = new BateriaSetupTab();
                    return bateria_tab;
                case 2:
                    OnMarkedClientsTab marked_clients_tab = new OnMarkedClientsTab();
                    return marked_clients_tab;
                case 3:
                    OnAllClientsTab on_all_clients_tab = new OnAllClientsTab();
                    return on_all_clients_tab;
                case 4:
                    EffectsListTab effects_list_tab = new EffectsListTab();
                    return effects_list_tab;
                case 5:
                    ClientsListTab tab3_tab = new ClientsListTab();
                    return tab3_tab;
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 6 total pages.
            return 6;
        }

        @Override
        public CharSequence getPageTitle(int in_position) {

            switch( in_position ){
                case 0:
                    return "server_tab";
                case 1:
                    return "bateria_setup_tab";
                case 2:
                    return "on_marked_clients_tab";
                case 3:
                    return "all_clients_tab";
                case 4:
                    return "effects_list_tab";
                case 5:
                    return "clients_list_tab";
            }
            return null;

        }
    }

    public native void initServer( String in_appPath );

}
