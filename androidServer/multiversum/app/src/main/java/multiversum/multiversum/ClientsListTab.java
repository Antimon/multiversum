package multiversum.multiversum;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ClientsListTab extends android.support.v4.app.Fragment{

    boolean _ui_updateRunnableActive = false;
    boolean _updateUI_inLoop = false;
    boolean _justCreatedView = true;

    boolean _serverRunning_old = false;
    boolean _serverRunning = false;

    ImageButton _refreshImageButton = null;

    ListView _clientsList = null;
    LinearLayout _linearLayout = null;

    TextView _numberOfClientsTextView = null;

    View _thisView = null;


    Bitmap _unknownBitmap = null;
    Bitmap _nullPointerBitmap = null;
    Bitmap _mestreBitmap = null;
    Bitmap _caixaBitmap = null;
    Bitmap _tamborimBitmap = null;
    Bitmap _repiniqueBitmap = null;
    Bitmap _chocalhoBitmap = null;
    Bitmap _primeraBitmap= null;
    Bitmap _segundaBitmap = null;
    Bitmap _tetieraBitmap = null;
    Bitmap _agogoBitmap = null;
    Bitmap _bullshitBitmap = null;


    Handler _handler = new Handler(){
        @Override
        public void handleMessage( Message msg ){
            //Log.d( "_debug", "entered BateriaSetupTab::handleMessage()" );
            super.handleMessage(msg);
            updateUI();

            updateDataFromServer();
            //buildList();

            //Log.d( "_debug", "leaving Clients::handleMessage()" );
        };
    };


    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }




    @Override
    public void onAttach(Context context) {
        //Log.d("_debug", "entering ClientsListTab::onAttach()");
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        //Log.d("_debug", "entering ClientsListTab::onCreate()");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        //Log.d("_debug", "entering ClientsListTab::onStart()");
        super.onStart();
        _updateUI_inLoop = true;
        start_updateUI_loop();
    }

    @Override
    public void onResume() {
        //Log.d("_debug", "entering ClientsListTab::onResume()");
        super.onResume();
        _handler.sendEmptyMessage(0);        //this is not absolutely necessary here, but it makes sure, that (after a swipe and back) the UI is in the old (running?) state immediately (without a small time where the gray "not-running" UI) can be seen
    }

    @Override
    public void onPause() {
        //Log.d("_debug", "entering ClientsListTab::onPause()");
        super.onPause();
    }

    @Override
    public void onStop() {
        //Log.d("_debug", "entering ClientsListTab::onStop()");
        super.onStop();
        _updateUI_inLoop = false;
    }

    @Override
    public void onDestroy() {
        //Log.d("_debug", "entering ClientsListTab::onDestroy()");
        super.onDestroy();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){

        //Log.d("_debug", "entering ClientsListTab::onCreateView()");
        super.onCreateView(inflater, container, savedInstanceState);
        final View output = inflater.inflate( R.layout.clients_list_tab, container, false );
        _thisView = output;

        _linearLayout = (LinearLayout) output.findViewById( R.id.clients_LinearLayout);

        _justCreatedView = true;

        //updateDataFromServer();
        //updateUI();


        _unknownBitmap = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.unknown);
        _unknownBitmap = Bitmap.createScaledBitmap(_unknownBitmap, 100, 100, true);
        _nullPointerBitmap = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.null_pointer);
        _nullPointerBitmap = Bitmap.createScaledBitmap(_nullPointerBitmap, 100, 100, true);
        _mestreBitmap = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.mestre);
        _mestreBitmap = Bitmap.createScaledBitmap(_mestreBitmap, 100, 100, true);
        _caixaBitmap = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.caixa);
        _caixaBitmap = Bitmap.createScaledBitmap(_caixaBitmap, 100, 100, true);
        _tamborimBitmap = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.tamborim);
        _tamborimBitmap = Bitmap.createScaledBitmap(_tamborimBitmap, 100, 100, true);
        _repiniqueBitmap = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.repinique);
        _repiniqueBitmap = Bitmap.createScaledBitmap(_repiniqueBitmap, 100, 100, true);
        _chocalhoBitmap = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.chocalho);
        _chocalhoBitmap = Bitmap.createScaledBitmap(_chocalhoBitmap, 100, 100, true);
        _primeraBitmap = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.surdo_primera);
        _primeraBitmap = Bitmap.createScaledBitmap(_primeraBitmap, 100, 100, true);
        _segundaBitmap = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.surdo_segunda);
        _segundaBitmap = Bitmap.createScaledBitmap(_segundaBitmap, 100, 100, true);
        _tetieraBitmap = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.surdo_tetiera);
        _tetieraBitmap = Bitmap.createScaledBitmap(_tetieraBitmap, 100, 100, true);
        _agogoBitmap = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.agogo);
        _agogoBitmap = Bitmap.createScaledBitmap(_agogoBitmap, 100, 100, true);
        _bullshitBitmap = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.bullshit);
        _bullshitBitmap = Bitmap.createScaledBitmap(_bullshitBitmap, 100, 100, true);

        _refreshImageButton = output.findViewById( R.id.refresh_imageButton );
        //_refreshImageButton.setVisibility( View.GONE ); // we actually refresh the list automatically every 1 second... -> not any more... it's better to refresh it manually

        _refreshImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //buildList( output );
                buildList();
            }
        });



        //buildList( output );
        buildList();

        _numberOfClientsTextView = (TextView) output.findViewById( R.id.clients_numberOfClients_TextView );
        //_numberOfClientsTextView.setText("foobar");
        updateNumberOfClients(output);

        return output;

    }


    private void updateNumberOfClients( View in_View ){
        int numberOfClients = getTotalNumberOfClients();
        _numberOfClientsTextView.setText( "" + numberOfClients );
    }


    private void buildList( /* View inputView */ ){
        int numberOfClients = collectClientsInformation();
        //TextView numberOfClientsTextView = (TextView)inputView.findViewById(R.id.clients_numberOfClients_TextView);
        TextView numberOfClientsTextView = (TextView) _thisView.findViewById(R.id.clients_numberOfClients_TextView);
        numberOfClientsTextView.setText( "" + numberOfClients );
        ClientsInformation[] listContent = new ClientsInformation[numberOfClients];
        for( int index = 0; index < numberOfClients; index++ ){
            String chipID = getChipIDFromSnapshotOnPosition(index);
            String version = getVersionFromSnapshotOnPosition(index);
            String signalStrength = getSignalStrengthFromSnapshotOnPosition(index);
            String lastSeen = getLastSeenFromSnapshotOnPosition(index);
            String instrument = getInstrumentFromSnapshotOnPosition(index);
            Bitmap instrumentPicture = getBitmapOfInstrument( instrument );
            String connected = getConnectedFromSnapshotOnPosition( index );
            String ipAdress = getIpAdressFromSnapshotOnPosition(index);
            String lastState = getLastStateFromSnapshotOnPosition(index);
            String chainLength = getChainLengthFromSnapshotOnPosition(index);
            String effect = getEffectFromSnapshotOnPosition(index);
            ClientsInformation temp_information = new ClientsInformation( chipID, version, signalStrength, lastSeen, instrument, instrumentPicture, connected, ipAdress, lastState, chainLength, effect );
            listContent[index] = temp_information;
        }
        //_clientsList = inputView.findViewById( R.id.clients_ListView );
        _clientsList = _thisView.findViewById( R.id.clients_ListView );
        ListAdapter clientsListAdapter = new CustomAdapterClientsList( getActivity(), listContent );
        _clientsList.setAdapter(clientsListAdapter);
        _clientsList.setOnItemClickListener( new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick( AdapterView<?> parent, View view, int position, long id ){
                Toast toast = Toast.makeText( getActivity(), "clicked", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }




    Bitmap getBitmapOfInstrument( String in_instrument ){
        Bitmap output;
        if( in_instrument.equals( "unknown" ) ){
            //output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.unknown);       // means: there is a client associated with this position, but it has no Instrument-information
            output = _unknownBitmap;
        }else if( in_instrument.equals( "null" ) ){
            //output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.null_pointer);  // means: there is no client associated with this position
            output = _nullPointerBitmap;
        }else if( in_instrument.equals( "mestre" ) ){
            //output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.mestre);
            output = _mestreBitmap;
        }else if( in_instrument.equals( "caixa" ) ){
            //output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.caixa);
            output = _caixaBitmap;
        }else if( in_instrument.equals( "tamborim" ) ){
            //output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.tamborim);
            output = _tamborimBitmap;
        }else if( in_instrument.equals( "repique" ) ){
            //output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.repinique);
            output = _repiniqueBitmap;
        }else if( in_instrument.equals( "chocalho" ) ){
            //output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.chocalho);
            output = _chocalhoBitmap;
        }else if( in_instrument.equals( "primera" ) ){
            //output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.surdo_primera);
            output = _primeraBitmap;
        }else if( in_instrument.equals( "segunda" ) ){
            //output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.surdo_segunda);
            output = _segundaBitmap;
        }else if( in_instrument.equals( "tetiera" ) ){
            //output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.surdo_tetiera);
            output = _tetieraBitmap;
        }else if( in_instrument.equals( "agogo" ) ){
            //output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.agogo);
            output = _agogoBitmap;
        }else{
            //Log.d( "_debug", "getInstrumentOnPosition() returned bullshit!" );
            //output = (Bitmap) BitmapFactory.decodeResource(getContext().getResources(), R.drawable.bullshit);      // means: the return-value doesn't match any other pattern.. -> bateria not initiated??
            output = _bullshitBitmap;
        }
        return output;
    }




    private void delay( int in_delay ){

        synchronized (this) {   // why ever i need this...
            try {
                wait(in_delay);
            } catch (Exception exception) {
                Log.d("_debug", " ... wait(" + in_delay + ") throwed exception: " + exception);
            }
        }

    }



    public  void start_updateUI_loop(){

        Runnable runnable = new Runnable(){
            @Override
            public void run(){

                while( _ui_updateRunnableActive ){  //to make absolutely sure, there is only one ui-update-thread
                    delay(300);
                    //Log.d("_debug", "ClientsListTab::start_updateUI_loop(): _ui_updateRunnableActive == true" );
                }

                _ui_updateRunnableActive = true;
                //delay(700 ); //this is just for a better feeling... we pretend the app must du a lot of stuff by introducing this delay here...
                updateDataFromServer();
                while( _updateUI_inLoop ){
                    //Log.d("_debug", "ClientsListTab::start_updateUI_loop(): calling _handler.sendEmptyMessage()");
                    _handler.sendEmptyMessage(0);
                    delay(3000 );   //wait 3 seconds
                }
                //Log.d("_debug", "ClientsListTab::start_updateUI_loop(): leaving ui-runnable!!!!!!!!!!!!!!!!!!!!!!!!!!");
                _ui_updateRunnableActive = false;
            }
        };
        if( _ui_updateRunnableActive == false ){    // after a veerry quick swipe to the non-neighbour-tab (e.g. Actions) and back it might be the case, that the runnable is still active (becuase it was sleeping). to avoid to start another one we check that here...
            //Log.d("_debug", "ClientsListTab: starting new ui-runnable.....................");
            Thread thread = new Thread(runnable);
            thread.start();
        }else{
            //Log.d("_debug", "ClientsListTab::start_updateUI_loop(): not starting a new runnable, because _ui_updateRunnableActive == true");
        }

    }

    public void updateDataFromServer(){

        _serverRunning_old = _serverRunning;
        _serverRunning = getCppServerRunning();

    }

    public void updateUI(){

        //Log.d( "_debug", "entering BateriaStatusTab::updateUI()" );

        if( (_serverRunning && _serverRunning_old == false) || (_justCreatedView && _serverRunning) ){
            _linearLayout.setBackgroundColor(Color.WHITE);
            _justCreatedView = false;
        }else if( _serverRunning == false && _serverRunning_old || (_justCreatedView && _serverRunning == false) ){
            _linearLayout.setBackgroundColor(Color.GRAY);
            _justCreatedView = false;
        }

    }



    public native boolean getCppServerRunning();
    public native int collectClientsInformation();  //returns the number of available ClientInformation-Packages
    public native String getChipIDFromSnapshotOnPosition(int in_position );
    public native String getVersionFromSnapshotOnPosition(int in_position );
    public native String getLastSeenFromSnapshotOnPosition(int in_position );
    public native String getSignalStrengthFromSnapshotOnPosition(int in_position );
    public native String getConnectedFromSnapshotOnPosition( int in_position );
    public native String getInstrumentFromSnapshotOnPosition( int in_position );
    public native String getIpAdressFromSnapshotOnPosition( int in_position );
    public native String getLastStateFromSnapshotOnPosition( int in_position );
    public native int getTotalNumberOfClients();
    public native String getChainLengthFromSnapshotOnPosition( int in_position );
    public native String getEffectFromSnapshotOnPosition( int in_position );

}