package multiversum.multiversum;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.widget.ImageView;

public class BateriaImageView {



    public BateriaImageView( ImageView in_imageView ){
        _imageView = in_imageView;


        _isInverted = false;

    }

    public void setBitmap( Bitmap in_bitmap ){
        _bitmap = in_bitmap;
        _imageView.setImageBitmap(_bitmap);
        _bitmapInverted =invertBitmap( in_bitmap );
    }

    public ImageView _imageView = null;

    public Bitmap _bitmap = null;

    public Bitmap _bitmapInverted = null;

    public boolean _isInverted = false;

    public void switchToInverted(){
        _imageView.setImageBitmap( _bitmapInverted );
        _isInverted = true;
    }

    public void switchToNormal(){
        _imageView.setImageBitmap( _bitmap );
        _isInverted = false;
    }

    private Bitmap invertBitmap(Bitmap in_bitmap ){
        ColorMatrix colorMatrix_Inverted = new ColorMatrix( new float[] { -1,  0,  0,  0, 255, 0, -1,  0,  0, 255, 0,  0, -1,  0, 255, 0,  0,  0,  1,   0 } );
        ColorFilter sephia = new ColorMatrixColorFilter( colorMatrix_Inverted );
        Bitmap output = Bitmap.createBitmap( in_bitmap.getWidth(), in_bitmap.getHeight(), Bitmap.Config.ARGB_8888 );
        Canvas canvas = new Canvas( output );
        Paint paint = new Paint();
        paint.setColorFilter( sephia );
        canvas.drawBitmap( in_bitmap, 0, 0, paint );
        return output;
    }


}
