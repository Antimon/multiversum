package multiversum.multiversum;


import android.graphics.Bitmap;

public class EffectsInformation {

    String _effectName;
    Bitmap _effectPicture;
    int _redValue;
    int _greenValue;
    int _blueValue;
    int _uvValue;
    boolean _stayOnColor;
    int _currentTriggerThreshold;
    int _speed;
    int _attack;
    int _release;
    boolean _active;
    Position _positionOfClientWithThisEffect;
    String _chipIDofClientWithThisEffect;

    public EffectsInformation( String in_effectName, int in_redValue, int in_greenValue, int in_blueValue, int in_uvValue, Bitmap in_effectPicture, boolean in_stayOnColor, int in_currentTriggerThreshold, int in_speed, int in_attack, int in_release, boolean in_active, int in_rowOfCientWithEffect, int in_colomnOfClientWithEffect, String in_chipIDofClientWithEffect ){
        _effectName = in_effectName;
        _effectPicture = in_effectPicture;
        _redValue = in_redValue;
        _greenValue = in_greenValue;
        _blueValue = in_blueValue;
        _uvValue = in_uvValue;
        _stayOnColor = in_stayOnColor;
        _currentTriggerThreshold = in_currentTriggerThreshold;
        _speed = in_speed;
        _attack = in_attack;
        _release = in_release;
        _active = in_active;
        _positionOfClientWithThisEffect = new Position( in_rowOfCientWithEffect, in_colomnOfClientWithEffect );
        _chipIDofClientWithThisEffect = in_chipIDofClientWithEffect;
    }

}
