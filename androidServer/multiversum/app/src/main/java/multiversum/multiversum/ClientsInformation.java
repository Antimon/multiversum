package multiversum.multiversum;

import android.graphics.Bitmap;

class ClientsInformation{

    String chipID;
    String version;
    String signalStrength;
    String lastSeen;
    Bitmap picture;
    String connected;
    String ipAdress;
    String instrument;
    String lastState;
    String chainLength;
    String effect;

    public ClientsInformation( String in_clientID, String in_version, String in_signalStrength, String in_lastSeen, String in_instrument, Bitmap in_picture, String in_connected, String in_ipAdress, String in_lastState, String in_chainLength, String in_effect ) {
        chipID = in_clientID;
        version = in_version;
        signalStrength = in_signalStrength;
        lastSeen = in_lastSeen;
        picture = in_picture;
        connected = in_connected;
        ipAdress = in_ipAdress.substring( 0, in_ipAdress.length() - 1 );      //removing the \n
        instrument = in_instrument;
        lastState = in_lastState;
        chainLength = in_chainLength;
        effect = in_effect;
    }

}