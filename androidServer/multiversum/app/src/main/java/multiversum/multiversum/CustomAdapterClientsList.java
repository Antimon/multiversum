package multiversum.multiversum;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

class CustomAdapterClientsList extends ArrayAdapter{

    ColorSet _currentColor;
    Toast _currentToast = null;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    public CustomAdapterClientsList( @NonNull Context context, ClientsInformation[] input ) {
        super( context, R.layout.row_clients_list, input );
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable final View convertView, @NonNull ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        LayoutInflater someInflater = LayoutInflater.from( getContext() );
        final View customView = someInflater.inflate( R.layout.row_clients_list, parent, false );

        final ClientsInformation singleInformation = (ClientsInformation) getItem( position );
        TextView chipIDTextView = (TextView) customView.findViewById(R.id.chipID_textView);
        chipIDTextView.setText(         "ChipID:     " + singleInformation.chipID );
        chipIDTextView.setTypeface(Typeface.MONOSPACE);

        ImageView instrumtentImageView = (ImageView) customView.findViewById(R.id.clientsList_instrument_imageView);
        instrumtentImageView.setImageBitmap( singleInformation.picture );

        TextView versionTextView = (TextView) customView.findViewById( R.id.version_textView );
        versionTextView.setText(        "Version:    " + singleInformation.version );
        versionTextView.setTypeface( Typeface.MONOSPACE );

        TextView signalStrengthTextView = (TextView) customView.findViewById( R.id.signalStrength_textView );
        signalStrengthTextView.setText( "Signal:     " + singleInformation.signalStrength);
        signalStrengthTextView.setTypeface( Typeface.MONOSPACE );

        TextView lastSeenTextView = (TextView) customView.findViewById( R.id.lastSeen_textView );
        lastSeenTextView.setText(       "Last Seen:  " + singleInformation.lastSeen );
        lastSeenTextView.setTypeface( Typeface.MONOSPACE );

        TextView connectedTextView = (TextView) customView.findViewById( R.id.connected_textView );
        connectedTextView.setText(      "online:     " + singleInformation.connected );
        connectedTextView.setTypeface( Typeface.MONOSPACE );

        TextView ipAdressTextView = (TextView) customView.findViewById( R.id.ipAdress_textView );
        ipAdressTextView.setText(       "IP:         " + singleInformation.ipAdress );
        //ipAdressTextView.setText(       "IP:         " );
        ipAdressTextView.setTypeface( Typeface.MONOSPACE );

        TextView lastState = (TextView) customView.findViewById(R.id.lastState_textView);
        lastState.setText(              "last state: " + singleInformation.lastState );
        lastState.setTypeface( Typeface.MONOSPACE );

        TextView chainLength = (TextView) customView.findViewById(R.id.chainLength_textView);
        chainLength.setText(            "strip len:  " + singleInformation.chainLength );
        chainLength.setTypeface( Typeface.MONOSPACE );

        TextView effect = (TextView) customView.findViewById(R.id.effect_textView);
        effect.setText(                 "effect:     " + singleInformation.effect );
        effect.setTypeface( Typeface.MONOSPACE );

        Button blinkButton = (Button) customView.findViewById( R.id.blink_button);
        blinkButton.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String chipID = singleInformation.chipID;
                makeClientWithIDblink( chipID );
            }
        });

        Button moreButton = (Button) customView.findViewById( R.id.more_button );
        moreButton.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                showMoreDialog( customView, singleInformation.chipID );
            }
        });

        final SeekBar uvSeekBar = (SeekBar) customView.findViewById( R.id.waterDrop_uv_seekBar);
        uvSeekBar.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int uvValue = uvSeekBar.getProgress();
                _currentColor.uv = uvValue;
                int uvPercent = (int) ( (  ((float)uvValue)/((float)255.0)  )*100 + 0.5);
                if( _currentToast != null ){
                    _currentToast.cancel();
                }
                _currentToast = Toast.makeText( customView.getContext(), "set UV to " + uvPercent + "%", Toast.LENGTH_SHORT );
                _currentToast.show();
                _currentColor.uv = uvValue;
                setClientToColor( singleInformation.chipID, _currentColor.red, _currentColor.green, _currentColor.blue, _currentColor.uv );
            }
        });
        return customView;
    }

    private void showMoreDialog( final View in_view, final String in_chipID ){
        final String[] possibleOptions = new String[]{"set Instrument", "set effect on this client", "show log", "reset", "send string direct to client", "set LED-strip-length"};
        AlertDialog.Builder builder = new AlertDialog.Builder( in_view.getContext() );
        builder.setTitle("wut do?");
        builder.setItems(possibleOptions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int in_clickedOnItem) {

                if( in_clickedOnItem == 0 ){
                    startInstrumentAssociation( in_view.getContext(), in_chipID );
                }else if( in_clickedOnItem == 1 ){
                    if( bateriaExists() ){
                        _currentToast.cancel();
                    }else{
                        if( _currentToast != null ){
                            _currentToast.cancel();
                        }
                        _currentToast = Toast.makeText( getContext(), "fail! create bateria first!", Toast.LENGTH_LONG );
                        _currentToast.show();
                    }
                }else if( in_clickedOnItem == 2 ){
                    showLog( in_chipID );
                }else if( in_clickedOnItem == 3 ){
                    if( reset( in_chipID ) ){
                        if( _currentToast != null ){
                            _currentToast.cancel();
                        }
                        _currentToast = Toast.makeText( in_view.getContext(), "resetting " + in_chipID, Toast.LENGTH_SHORT);
                        _currentToast.show();
                    }else{
                        if( _currentToast != null ){
                            _currentToast.cancel();
                        }
                        _currentToast = Toast.makeText( in_view.getContext(), "failed to reset " + in_chipID, Toast.LENGTH_SHORT);
                        _currentToast.show();
                    }
                }else if( in_clickedOnItem == 4 ) {
                    showSendStringDirectlyToClientDialog(in_chipID);
                }else if( in_clickedOnItem == 5 ){
                    showSetChainLengthDialog( in_chipID );
                }else{
                    Log.d("_debug", "showMoreDialog(): strange behaviour");
                }
            }
        });
        builder.show();
    }

    private void showSetChainLengthDialog( final String in_chipID ){
        AlertDialog.Builder builder = new AlertDialog.Builder( getContext() );
        builder.setTitle("set LED-strip Length");

        // Set up the input
        final EditText input = new EditText( getContext() );
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        //input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int newLength = Integer.parseInt( input.getText().toString() );
                if( newLength <= 1 || newLength > 170 ){
                    if( _currentToast != null ){
                        _currentToast.cancel();
                    }
                    _currentToast = Toast.makeText( getContext(), "fail! strip-length must be at least 1 (default) and less then 170", Toast.LENGTH_LONG );
                    _currentToast.show();
                }else{
                    setLEDchainLength( newLength, in_chipID );
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void showSendStringDirectlyToClientDialog( final String in_chipID ){
        AlertDialog.Builder builder = new AlertDialog.Builder( getContext() );
        builder.setTitle("send string directly to client");

        // Set up the input
        final EditText input = new EditText( getContext() );
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        //input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String toSendString = input.getText().toString();
                sendStringDirectlyToClientWithID(toSendString, in_chipID);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void setEffectOnThisClient(final String in_chipID ){

        //boolean output = false;
        final String[] possibleOptions = new String[]{"waterDrop", "immediately change of Color",};
        AlertDialog.Builder builder = new AlertDialog.Builder( getContext() );
        builder.setTitle("select effect?");
        builder.setItems(possibleOptions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int in_clickedOnItem) {
                if( in_clickedOnItem == 0 ){
                    Intent waterDropSetupIntend = new Intent( getContext(), WaterDropEffectSetup.class );
                    waterDropSetupIntend.putExtra( "row", -1 ); // this of course makes no sense, but by that, we signalize to the WaterDropEfectSetup, that we do not know any position, but do know the chipID
                    waterDropSetupIntend.putExtra( "colomn", -1 );
                    waterDropSetupIntend.putExtra( "chipID", in_chipID );
                    getContext().startActivity( waterDropSetupIntend );
                    //output = true;
                }else if( in_clickedOnItem == 1 ){
                    Toast.makeText( getContext(), "CustomAdapterClientsList::setEffectOnThisClient(): empty function", Toast.LENGTH_SHORT).show();
                }else{
                    Log.d("_debug", "CustomAdapterClientsList::setEffectOnThisClient(): strange behaviour");
                }

            }
        });
        builder.show();
        //return output;
    }

    private void showLog( String in_chipID ){
        String completeLog = "";
        lockLogOnClient();
        int logSize = getLogSize( in_chipID );
        prepareRead( in_chipID );
        for( int index = 0; index < logSize; index++ ){
            completeLog += getNextLogLine(in_chipID) + "\n";
        }
        unlockLog( in_chipID );
        Intent logViewIntend = new Intent( getContext(), LogView.class );
        logViewIntend.putExtra( "log", completeLog );
        getContext().startActivity( logViewIntend );
    }

    private void startInstrumentAssociation( final Context in_context, final String in_chipID ){
        final String[] instrumentArray = new String[]{"unknown", "mestre", "caixa", "tamborim", "repique", "chocalho", "primera", "segunda", "tetiera", "agogo"};
        AlertDialog.Builder builder = new AlertDialog.Builder( in_context );
        builder.setTitle("Pick an Instrument");
        builder.setItems(instrumentArray, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int in_clickedOnItem) {
                String instrumentString = "";
                if( instrumentArray[in_clickedOnItem].equals( "unknown" ) ){
                    instrumentString = "unknown";
                }else if( instrumentArray[in_clickedOnItem].equals( "mestre" ) ){
                    instrumentString = "mestre";
                }else if( instrumentArray[in_clickedOnItem].equals( "caixa" ) ){
                    instrumentString = "caixa";
                }else if( instrumentArray[in_clickedOnItem].equals( "tamborim" ) ){
                    instrumentString = "tamborim";
                }else if( instrumentArray[in_clickedOnItem].equals( "repique" ) ){
                    instrumentString = "repique";
                }else if( instrumentArray[in_clickedOnItem].equals( "chocalho" ) ){
                    instrumentString = "chocalho";
                }else if( instrumentArray[in_clickedOnItem].equals( "primera" ) ){
                    instrumentString = "primera";
                }else if( instrumentArray[in_clickedOnItem].equals( "segunda" ) ){
                    instrumentString = "segunda";
                }else if( instrumentArray[in_clickedOnItem].equals( "tetiera" ) ){
                    instrumentString = "tetiera";
                }else if(instrumentArray[in_clickedOnItem].equals( "agogo" ) ){
                    instrumentString = "agogo";
                }else{
                    Log.d("_debug", "unknown input...");
                    instrumentString = "unknown input";
                }
                boolean successfullySet = setInstrumentOnFromChipID( in_chipID, instrumentString);
                if( successfullySet ){
                    Toast.makeText( in_context, "new instrument set...", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText( in_context, "failed to set instrument... bateria not created or client not assigned to a position!", Toast.LENGTH_LONG).show();
                }
            }
        });
        builder.show();
    }

    public native void makeClientWithIDblink( String in_chipID );
    public native boolean setInstrumentOnFromChipID( String in_chipID, String in_instrument );
    public native boolean reset( String in_chipID );
    public native boolean setClientToColor( String in_chipID, int in_red, int in_green, int in_blue, int in_uv );
    public native void lockLogOnClient();
    public native int getLogSize( String in_chipID );
    public native void prepareRead( String in_chipID );
    public native String getNextLogLine( String in_chipID );
    public native void unlockLog( String in_chipID );
    public native boolean bateriaExists();
    public native void sendStringDirectlyToClientWithID( String in_toSendString, String in_chipID );
    public native void setLEDchainLength( int in_newLength, String in_chipID );

}
