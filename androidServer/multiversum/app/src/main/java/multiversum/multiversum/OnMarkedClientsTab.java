package multiversum.multiversum;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;


public class OnMarkedClientsTab extends android.support.v4.app.Fragment {


    RelativeLayout _relativeLayout = null;
    Button _pickColorButton = null;

    boolean _justCreatedView = false;
    boolean _ui_updateRunnableActive = false;
    boolean _updateUI_inLoop = false;

    boolean _serverRunning_old = false;
    boolean _serverRunning = false;

    SeekBar _uvSeekBar = null;
    Toast _currentToast = null;

    ColorSet _currentColor;

    TextView _redTextView = null;
    TextView _greenTextView = null;
    TextView _blueTextView = null;
    TextView _uvTextView = null;



    Handler _handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            //Log.d( "_debug", "entered BateriaSetupTab::handleMessage()" );
            super.handleMessage(msg);
            updateUI();

            updateDataFromServer();

            //Log.d( "_debug", "leaving BateriaSetupTab::handleMessage()" );
        };
    };

    public void updateDataFromServer(){
        _serverRunning_old = _serverRunning;
        _serverRunning = getCppServerRunning();
    }

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }


    @Override
    public void onAttach(Context context) {
        //Log.d("_debug", "entering OnMarkedClientsTab::onAttach()");
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        //Log.d("_debug", "entering OnMarkedClientsTab::onCreate()");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        //Log.d("_debug", "entering ActionTab::onStart()");

        super.onStart();

        _updateUI_inLoop = true;
        start_updateUI_loop();
    }

    @Override
    public void onResume() {
        //Log.d("_debug", "entering OnMarkedClientsTab::onResume()");
        super.onResume();
        _handler.sendEmptyMessage(0);        //this is not absolutely necessary here, but it makes sure, that (after a swipe and back) the UI is in the old (running?) state immediately (without a small time where the gray "not-running" UI) can be seen
    }

    @Override
    public void onPause() {
        //Log.d("_debug", "entering OnMarkedClientsTab::onPause()");
        super.onPause();
    }

    @Override
    public void onStop() {
        //Log.d("_debug", "entering OnMarkedClientsTab::onStop()");
        super.onStop();
        _updateUI_inLoop = false;
    }

    @Override
    public void onDestroy() {
        //Log.d("_debug", "entering OnMarkedClientsTab::onDestroy()");
        super.onDestroy();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //Log.d("_debug", "entering OnMarkedClientsTab::onCreateView()");
        super.onCreateView(inflater, container, savedInstanceState);
        _justCreatedView = true;
        View output = inflater.inflate( R.layout.on_marked_clients_tab, container, false );
        _pickColorButton = (Button) output.findViewById(R.id.pickColor_Button);
        _pickColorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showColorPicker();
            }
        });

        _currentColor = new ColorSet();


        _redTextView = (TextView) output.findViewById( R.id.currentColor_red_textView );
        _greenTextView = (TextView) output.findViewById( R.id.currentColor_green_textView );
        _blueTextView = (TextView) output.findViewById( R.id.currentColor_blue_textView );
        _uvTextView = (TextView) output.findViewById( R.id.currentColor_uv_textView );

        initSeekBar( output );



        _relativeLayout = (RelativeLayout) output.findViewById( R.id.onMarked_content_relativeLayout );
        _relativeLayout.setBackgroundColor( Color.GRAY );

        return output;
    }

    public void initSeekBar( View in_view ){
        _uvSeekBar = (SeekBar) in_view.findViewById( R.id.waterDrop_uv_seekBar);
        _uvSeekBar.setMax(1023);    // the uv value has more resolution then the normal rgb-values
        _uvSeekBar.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Toast.makeText( getActivity(), "uvSeekBar: onClickListener", Toast.LENGTH_LONG ).show();
                //Log.d("_debug", "uvSeekBar: onClickListener");
            }
        });
        _uvSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //Toast.makeText( getActivity(), "uvSeekBar: onProgressChanged: progress: " + progress, Toast.LENGTH_LONG ).show();
                //Log.d("_debug", "uvSeekBar: onProgressChanged: progress: " + progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //Toast.makeText( getActivity(), "uvSeekBar: onStartTrackingTouch", Toast.LENGTH_LONG ).show();
                //Log.d("_debug", "uvSeekBar: onStartTrackingTouch");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int uvValue = _uvSeekBar.getProgress();
                //int uvPercent = (int) ( (  ((float)uvValue)/((float)255.0)  )*100 + 0.5);

                updateCurrentColor_uv( uvValue );
                setMarkedClientsToColor( _currentColor.red, _currentColor.green, _currentColor.blue, _currentColor.uv );

            }
        });
    }


    public void showColorPicker(){
        // check: https://android-arsenal.com/details/1/1693
        ColorPickerDialogBuilder.with( getActivity() ).setTitle("Choose color").initialColor( Color.WHITE ).wheelType(ColorPickerView.WHEEL_TYPE.FLOWER).density(10).setOnColorSelectedListener(new OnColorSelectedListener(){
            @Override
            public void onColorSelected(int in_selectedColor) {
                String colorHexString = "" + ( Integer.toHexString(in_selectedColor) ).toString().substring(2).toUpperCase();
                Toast.makeText( getActivity(), "onColorSelected: " + colorHexString, Toast.LENGTH_SHORT).show();
                int color[] = getRGB( colorHexString );
                //Log.d("_debug", "R: " + color[0] + " G: " + color[1] + " B: " + color[2]);
                //doSomethingWithColor( color );
                updateCurrentColor_rgb( color[0], color[1], color[2] );
                setMarkedClientsToColor( _currentColor.red, _currentColor.green, _currentColor.blue, _currentColor.uv );
            }
        }).setPositiveButton("ok", new ColorPickerClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int in_selectedColor, Integer[] allColors) {
                //doSomethingWithColor(in_selectedColor);
            }
        }).setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which){
            }
        }).build().show();
    }



    public void updateCurrentColor_rgb( int in_red, int in_green, int in_blue ){
        _currentColor.red = in_red;
        _currentColor.green = in_green;
        _currentColor.blue = in_blue;

        int percentageRed = (int) ( (  ((float)in_red)/((float)255.0)  )*100 + 0.5);
        int percentageGreen = (int) ( (  ((float)in_green)/((float)255.0)  )*100 + 0.5);
        int percentageBlue = (int) ( (  ((float)in_blue)/((float)255.0)  )*100 + 0.5);

        _redTextView.setText(percentageRed + "%");
        _greenTextView.setText(percentageGreen + "%");
        _blueTextView.setText(percentageBlue + "%");
    }

    public void updateCurrentColor_uv( int in_uv ){
        _currentColor.uv = in_uv;

        int percentageUV = (int) ( (  ((float)in_uv)/((float)1023.0)  )*100 + 0.5);

        _uvTextView.setText(percentageUV + "%");
    }

    public static int[] getRGB(final String rgb){
        final int[] ret = new int[3];
        for (int i = 0; i < 3; i++)        {
            ret[i] = Integer.parseInt(rgb.substring(i * 2, i * 2 + 2), 16);
        }
        return ret;
    }



    public  void start_updateUI_loop(){

        Runnable runnable = new Runnable(){
            @Override
            public void run(){

                while( _ui_updateRunnableActive ){  //to make absolutely sure, there is only one ui-update-thread
                    delay(300);
                    //Log.d("_debug", "OnMarkedClientsTab::start_updateUI_loop(): _ui_updateRunnableActive == true" );
                }

                _ui_updateRunnableActive = true;
                //delay(700 ); //this is just for a better feeling... we pretend the app must du a lot of stuff by introducing this delay here...
                updateDataFromServer();
                while( _updateUI_inLoop ){
                    //Log.d("_debug", "OnMarkedClientsTab::start_updateUI_loop(): calling _handler.sendEmptyMessage()");
                    _handler.sendEmptyMessage(0);
                    delay(1000 );   //wait 1 second
                }
                //Log.d("_debug", "OnMarkedClientsTab::start_updateUI_loop(): leaving ui-runnable!!!!!!!!!!!!!!!!!!!!!!!!!!");
                _ui_updateRunnableActive = false;
            }
        };
        if( _ui_updateRunnableActive == false ){    // after a veerry quick swipe to the non-neighbour-tab (e.g. Actions) and back it might be the case, that the runnable is still active (becuase it was sleeping). to avoid to start another one we check that here...
            //Log.d("_debug", "OnMarkedClientsTab: starting new ui-runnable.....................");
            Thread thread = new Thread(runnable);
            thread.start();
        }else{
            //Log.d("_debug", "BateriaSetupTab::start_updateUI_loop(): not starting a new runnable, because _ui_updateRunnableActive == true");
        }
    }

    public void updateUI(){
        //Log.d( "_debug", "entering BateriaStatusTab::updateUI()" );
        if( (_serverRunning && _serverRunning_old == false) || (_justCreatedView && _serverRunning) ){
            _relativeLayout.setBackgroundColor(Color.WHITE);
            _justCreatedView = false;
        }else if( _serverRunning == false && _serverRunning_old || (_justCreatedView && _serverRunning == false) ){
            _relativeLayout.setBackgroundColor(Color.GRAY);
            _justCreatedView = false;
        }
    }

    private void delay( int in_delay ){
        synchronized (this) {   // why ever i need this...
            try {
                wait(in_delay);
            } catch (Exception exception) {
                Log.d("_debug", " ... wait(" + in_delay + ") throwed exception: " + exception);
            }
        }
    }

    public native boolean setMarkedClientsToColor( int in_red, int in_green, int in_blue, int in_uv );
    public native boolean getCppServerRunning();

}