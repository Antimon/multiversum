
#include <ESP8266WiFi.h>
#include <Adafruit_NeoPixel.h>
#include <EEPROM.h>
#include <vector>

using namespace std;

// #define STATUS_LED 12

#define NEOPIXEL_LED 5  // for ESP8266-12F
//#define NEOPIXEL_LED 2  // for ESP8266-01

#define UV_LED 13
#define TRIGGER_THRESHOLD_OUT 12
#define TRIGGER_INPUT 2

#define BUTTON_0 4
#define BUTTON_TIMEOUT 30    // milliseconds
//#define CHAIN_LENGTH 1
#define DEBUG false       // if true -> serial-output and a slower cycle
#define NETWORK_NAME "multiversum"
#define NETWORK_KEY "4815162342"
#define SERVER_IP "192.168.43.2"  // ip on my smartphone
//#define SERVER_IP "192.168.43.102"  // for debugging, my laptop is server...

// addresses of variable in eeprom to which is used as a bool-value
#define EEPROM_MAKE_SIGNALS 0                   // the adress of a bool-value! not the value itself!
#define EEPROM_DO_DEEP_SLEEP_IN_LOOP 2          // also the adress of a bool-value
#define EEPROM_DEEP_SLEEP_FOR_SECONDS 4         // watch out! this is not a value in seconds. this is the position of a byte in the eeprom, which stores the value in seconds
#define EEPROM_DO_WIFI_FORCED_SLEEP_IN_LOOP 6      // again: the adress of a bool value
#define EEPROM_WIFI_FORCED_SLEEP_FOR_SECONDS 8  // a value in seconds on position 8 of the eeprom
//#define EEPROM_CHAIN_LENGTH 10  // an integer-value on position 10 of the eeprom

#define USE_PING_TIME_TO_CHECK_DISCONNECTION true //if false, it will check the return-value of socket-writing-function
// it seems, it works better with this equal 'true'

#define MIN_TIME_BETWEEN_PINGS 2000 //milliseconds

#define SERVER_TIMEOUT 15000    // milliseconds! after this time since last ping from server is gone, this client will reconnect (wifi and server-connection)

//! took that from: https://learn.adafrluit.com/adafruit-neopixel-uberguide/arduino-library
// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)

//Adafruit_NeoPixel strip = NULL;
int chainLength = 1;
Adafruit_NeoPixel strip = Adafruit_NeoPixel( 170, NEOPIXEL_LED, NEO_GRB + NEO_KHZ800 );  // it seems, it is not possible to init the strip in the setup(). that would be nice, becase that is the place where we could call getChainLength(), which returned the chain length which is saved in the eeprom and can be set by smartphone-app. but that is not possible... so we init more then we possilby have and only use what is set by smartphone in eeprom
                                                                                       // according to this: https://learn.adafruit.com/neopixel-painter/test-neopixel-strip
                                                                                       // there is an upper limit of 170


typedef struct{
    int red = 0;
    int green = 0;
    int blue = 0;
    int uv = 0;
}colorSet;

typedef enum{
    up,
    down
}fadeDirectionEnum;

typedef struct{
    fadeDirectionEnum red;
    fadeDirectionEnum green;
    fadeDirectionEnum blue;
    fadeDirectionEnum uv;
}fadeDirectionStruct;

typedef struct{
    float red = 0;
    float green = 0;
    float blue = 0;
    float uv = 0;
}fadeDeltaStruct;

typedef struct{
    fadeDeltaStruct fadeDelta;
    int steps = 0;
    int currentStepOfCurrentFade = 0;
    fadeDirectionStruct fadeDirection;
    colorSet destinationColor;
    colorSet startColor;
    int sustain = 0;
}fade;

typedef struct{
    colorSet currentColor;
    vector<fade> fades;
    int steps;
    long timeLastFadeHasBeenFinished = 0;
    long timeCurrentStateShouldSustainForAtLeast = 0;
}colorStateStruct;

//colorStateStruct pixels[CHAIN_LENGTH];    // this saves the states and future fades of the pixels (LEDs)
vector<colorStateStruct> pixels;

WiFiClient client;
byte WiFistatus = WL_DISCONNECTED;
int tryesToConnectToWifi = 0;
int tryesToConnectToServer = 0;
IPAddress ownIP = (0, 0, 0, 0);
bool connectedToWifi = false;
bool connectedToServer = false;
bool connectedToServer_old = false;
#if USE_PING_TIME_TO_CHECK_DISCONNECTION
long timeLastPingReceived = 0;
#endif
long timeLastPingSent = 0;
String messageBuffer = "";
int numberOfCompleteMessagesInMessgeBuffer = 0;
String wifiSignalStrength = "";
int SignalStrengthUpdateCounter = 0;

unsigned long button0_lastPressed;
bool button0_oldState = false;

int keepWifiOffAtLeastUntil = 0;
bool wifiIsOn = true;

bool doFadesOnTrigger = false;
bool sendTriggerSignalsToServer = true;
bool trigger_oldState = false;
bool trigger_state = false;

void eepromWriteInt(int in_adr, int in_value){
    byte low, high;
    low = in_value & 0xFF;
    high = (in_value >> 8) & 0xFF;
    EEPROM.write(in_adr, low);
    EEPROM.write(in_adr + 1, high);
    EEPROM.commit();  // or EEPROM.end() ????
}

int eepromReadInt(int in_adr){
    byte low, high;
    low = EEPROM.read(in_adr);
    high = EEPROM.read(in_adr + 1);
    return low + ((high << 8) & 0xFF00);
}

bool getMakeSignals(){
    bool output = eepromReadInt( EEPROM_MAKE_SIGNALS );
    //return false;
    return output;
    
}

void setMakeSignals( bool in_state ){
    if( getMakeSignals() != in_state ){ // the flash rom might be happy if we don't write to often..
        //EEPROM.write( EEPROM_MAKE_SIGNALS, in_state );
        eepromWriteInt( EEPROM_MAKE_SIGNALS, in_state );
    }
}

bool getDoDeepSleepInLoop(){
    //return (bool)EEPROM.read( EEPROM_DO_DEEP_SLEEP_IN_LOOP );
    return (bool)eepromReadInt( EEPROM_DO_DEEP_SLEEP_IN_LOOP );
}

void setDoDeepSleepInLoop( bool in_state ){
    Serial.println("entering setDoDeepSleepInLoop()");
    //delay(3000);
    if( getDoDeepSleepInLoop() != in_state ){
        //EEPROM.write( EEPROM_DO_DEEP_SLEEP_IN_LOOP, in_state );
        eepromWriteInt( EEPROM_DO_DEEP_SLEEP_IN_LOOP, (int)in_state );
    }
}

int getDeepSleepInLoopForSeconds(){
    // return (int)EEPROM.read( EEPROM_DEEP_SLEEP_FOR_SECONDS );
    return eepromReadInt( EEPROM_DEEP_SLEEP_FOR_SECONDS );
}

void setDeepSleepInLoopForSeconds( int in_seconds ){
    Serial.print("entering setDeepSleepInLoopForSeconds() ... in_seconds: ");
    Serial.println(in_seconds);
    if( getDeepSleepInLoopForSeconds() != in_seconds ){
        Serial.println("getDeepSleepInLoopForSeconds() != in_seconds");
        eepromWriteInt( EEPROM_DEEP_SLEEP_FOR_SECONDS, in_seconds );
    }
    Serial.println("leaving setDeepSleepInLoopForSeconds()");
}

int getWifiForcedSleepInLoopForSeconds(){
    int output = eepromReadInt( EEPROM_WIFI_FORCED_SLEEP_FOR_SECONDS );
    return output;;
}

void setWifiForcedSleepInLoopForSeconds( int in_seconds ){
    Serial.print("entering setWifiForcedSleepInLoopForSeconds() ... in_seconds: ");
    Serial.println(in_seconds);
    if( getWifiForcedSleepInLoopForSeconds() != in_seconds ){
        eepromWriteInt( EEPROM_WIFI_FORCED_SLEEP_FOR_SECONDS, in_seconds );
    }
}

bool getDoWifiForcedSleepInLoop(){
    return (bool)eepromReadInt( EEPROM_DO_WIFI_FORCED_SLEEP_IN_LOOP );
}

void setDoWifiForcedSleepInLoop( bool in_state ){
    Serial.print("entering setDoWifiForcedSleepInLoop() ... in_state: " );
    Serial.println(in_state);
    if( getDoWifiForcedSleepInLoop() != in_state ){
        eepromWriteInt( EEPROM_DO_WIFI_FORCED_SLEEP_IN_LOOP, (int)in_state );
    }
}

/*
int getChainLength(){
    return (int)eepromReadInt( EEPROM_CHAIN_LENGTH );
    //return 1;
}
*/

/*
void setChainLength( int in_chainLength ){
    Serial.print("entering setChainLength() ... in_chainLength: " );
    Serial.println(in_chainLength);
    if( getChainLength() != in_chainLength ){
        eepromWriteInt( EEPROM_CHAIN_LENGTH, in_chainLength );
    }
}
*/

void setup(){
  
    delay(5000);
    Serial.begin(9600);
    Serial.print("Serial initialized. this is a client. chipID: ");
    Serial.println( ESP.getChipId() );
    delay(3000);
  
    Serial.println("calling EEPROM.begin(128)");
    EEPROM.begin(128);

    randomSeed( analogRead(0) );    //need this, because it will be possible to set random values for colors
  
    pinMode(BUTTON_0, INPUT);
  
    // if you experience connection-problems, try with this:
    /*
      WiFi.mode(WIFI_STA);
      Serial.println("calling WiFi.setAutoConnect()");
      WiFi.setAutoConnect(false);
      Serial.println("calling WiFi.setAutoReconnect()");
      WiFi.setAutoReconnect(false);
      Serial.println("calling WiFi.disconnect()");
      WiFi.disconnect();
    */

    buildPixelVector();
        
    //strip = Adafruit_NeoPixel( chainLength, NEOPIXEL_LED, NEO_GRB + NEO_KHZ800 );
    strip.begin();
    strip.setPixelColor( 0, 0, 0, 0, 0 );
    strip.show();
    
    
    // now we do something dangerous.. we abuse the UV_LED-pin as an input (befor we switch the mode to output). we do that to check for being low (-> pin is connected to ground on the board, via a jumper etc.)... if low, we switch the makeSignal to true (this variable should be stored in the flash in order to stay the same even after a reboot). then we keep waiting in a while loop for the pin to be un-connected to ground again... to signalize that, we do a blinking signal of the neopixel-led...
    // this is some kind of emergency-way to get signals, even if the signals got turned off...
    Serial.println("checking eeprom-reset");
    pinMode( UV_LED, INPUT );
    bool doReset = false;
    while( digitalRead(UV_LED) == true ){
        Serial.println("digitalRead(UV_LED) == true");
        setMakeSignals( true );
        setDoDeepSleepInLoop(false);
        setDoWifiForcedSleepInLoop(false);
        //setChainLength( 1 );
        Serial.print("getDoWifiForcedSleepInLoop(): ");
        Serial.println( getDoWifiForcedSleepInLoop() );
        Serial.print("getDoDeepSleepInLoop(): ");
        Serial.println( getDoDeepSleepInLoop() );
        Serial.println( "calling forceSleepWake()" );
        WiFi.forceSleepWake();
        Serial.println( "calling setPixelColor()" );
        strip.setPixelColor(  0, 255, 255, 255 );
        strip.show();
        delay(1000);
        strip.setPixelColor(  0, 0, 0, 0 );
        strip.show();
        delay(1000);
        doReset = true;
    }
    delay(1000);
    if( doReset ){
        reset();
    }
    
    pinMode(UV_LED, OUTPUT);

    //Serial.print("calling makeWakeUpSignal()");
    makeWakeUpSignal();
    delay(1000);
    
    //setDoDeepSleepInLoop(false);
    
    Serial.print("getDoWifiForcedSleepInLoop(): ");
    Serial.println(getDoWifiForcedSleepInLoop());
    Serial.print("getDoDeepSleepInLoop(): ");
    Serial.println(getDoDeepSleepInLoop());

    pinMode( TRIGGER_THRESHOLD_OUT, OUTPUT );
    pinMode( TRIGGER_INPUT, INPUT );
}

void buildPixelVector(){
    Serial.println("building pixel-vector");
    //int chainLength = getChainLength();
    pixels.clear();
    for( size_t index = 0; index < chainLength; index++ ){
        colorStateStruct temp;
        pixels.push_back(temp);
    }
    
}

void reset(){
    Serial.println("entering reset()");
    makeSignalOfDeath();
    writeServer("@state;reset#");
    delay(2000);
  
    //maybe this?
    //client.close();
    
    /*
      digitalWrite( 12, LOW );
      delay(1000);
    */
  
    ESP.reset();
    delay(2000);
    Serial.println("leaving reset()");  //hopefully we never reach this..
}

void doDeepSleepForSeconds( int in_seconds ){
    Serial.print("entering doDeepSleepForSeconds(): ... in_seconds: ");
    Serial.println(in_seconds);
    writeServer("@state;deepSleep");
    delay(1000);
    makeSignalForDeepSleep();
    ESP.deepSleep( in_seconds * 1000000, WAKE_RF_DEFAULT );
    delay(100); // seems to be important acording to this: https://www.youtube.com/watch?v=6SdyImetbp8
}

void doWifiForcedSleepForSeconds(  int in_seconds ){
    Serial.print("entering doWifiForcedSleepForSeconds() ... in_seconds: ");
    Serial.println( in_seconds );
    makeSignalForTurningWifiOff();
    writeServer("@state;wifiForcedSleep");
    client.stop();                  // we will lose the connection to the server anyway... so we better close the socket right now, hu?
    WiFi.forceSleepBegin();
    delay(100); // this seems to be very important for a proper forcedSleep
    wifiIsOn = false;
    long currentTime = millis();
    Serial.print("currentTime: ");
    Serial.println(currentTime);
    keepWifiOffAtLeastUntil = currentTime + in_seconds * 1000;
    Serial.print("keepWifiOffAtLeastUntil: ");
    Serial.println(keepWifiOffAtLeastUntil);
    Serial.println("leaving turnWifiOffForSeconds()");
}

void wakeUpWifiForcedSleep(){
    WiFi.forceSleepWake();
    delay(100); //seems to be very important...
    makeSignalForTurningWifiOn();
    delay(100); // probably not necessary here...
    //WiFi.mode(WIFI_STA); // better? -> not sure! .... naaaaaaa
    wifiIsOn = true;
    writeServer("@state;up");
}

void makeSignalOfDeath(){
    //Serial.println("entering makeSignalOfDeath()");
    if( getMakeSignals() ){
        strip.setPixelColor(  0, 0, 255, 0, 0);
        strip.show();
        delay(100);
        strip.setPixelColor( 0, 0, 0, 0, 0);
        strip.show();
        delay(100);
        strip.setPixelColor(  0,  0, 255, 0, 0);
        strip.show();
        delay(100);
        strip.setPixelColor( 0, 0, 0, 0, 0);
        strip.show();
        delay(200);
        strip.setPixelColor( 0, 0, 255, 0, 0);
        strip.show();
        delay(300);
        strip.setPixelColor( 0, 0, 0, 0, 0);
        strip.show();
        delay(400);
        strip.setPixelColor( 0, 0, 255, 0, 0);
        strip.show();
        delay(500);
        strip.setPixelColor( 0, 0, 0, 0, 0);
        strip.show();
        delay(600);
        strip.setPixelColor(  0, 0, 255, 0, 0);
        strip.show();
        delay(700);
        strip.setPixelColor( 0, 0, 0, 0, 0);
        strip.show();
        delay(800);
        strip.setPixelColor(  0, 0, 255, 0, 0);
        strip.show();
        delay(900);
        strip.setPixelColor( 0, 0, 0, 0, 0);
        strip.show();
        delay(1000);
        strip.setPixelColor(  0, 0, 255,  0, 0);
        strip.show();
        delay(1100);
        strip.setPixelColor( 0, 0, 0, 0, 0);
        strip.show();
    }
    //Serial.println("leaving makeSignalOfDeath()");
}

void makeSignalForLostServerConnection(){
    Serial.println("entering makeSignalForLostServerConnection()");
    if( getMakeSignals() ){
        for( int times = 0; times < 1; times++ ){
            //Serial.print("times: ");
            //Serial.println(times);
            for( int value = 255; value >= 0; value-- ){
                strip.setPixelColor( 0, 0, value, value, 0);
                strip.show();
                delay(10);
            }
            delay(100);
        }
        strip.setPixelColor( 0, 0, 0, 0, 0);
        strip.show();
    }
    Serial.println("leaving makeSignalForLostServerConnection()");
}

void connectToWifi(){
    Serial.print("enteirng connectToWifi() .... WiFistatus: ");
    Serial.println( WiFistatus );
  
    //Serial.println("calling WiFi.disconnect()");
    //WiFi.disconnect();
    delay(1000);
    
    Serial.println("trying to re-connect... calling WiFi.begin()!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    WiFistatus = WiFi.begin( NETWORK_NAME, NETWORK_KEY );
    Serial.print("WiFistatus: " );
    Serial.println(WiFistatus);
  
    // i know, this is a dirty hack. but i had the problem, that the display wouldn't reconnect to the detecter, once it has lost the connection.
    tryesToConnectToWifi++;
    Serial.print("tryesToConnectToWifi: ");
    Serial.println(tryesToConnectToWifi);
    if( tryesToConnectToWifi == 20 ){
        Serial.println("tryesToConnectToWifi == 20 reached");
        tryesToConnectToWifi = 0;
        Serial.print("getDoWifiForcedSleepInLoop(): ");
        Serial.println( getDoWifiForcedSleepInLoop() );
        if( getDoWifiForcedSleepInLoop() ){
            Serial.print("calling doWifiForcedSleepForSeconds() with value of: ");
            Serial.println( getWifiForcedSleepInLoopForSeconds() );
            doWifiForcedSleepForSeconds( getWifiForcedSleepInLoopForSeconds() );
        }else{
            Serial.println("calling reset(), because of to many tryes to reconnect to WiFi and no Wifi-Forced-Sleep is set...");
            delay(2000);
            reset();
        }
    }  
    Serial.println("leaving connectToWifi()");
}

void makeWakeUpSignal(){
    #if DEBUG
    Serial.println("entering makeWakeUpSignal()");
    #endif
  
    if( getMakeSignals() ){  //probably no need here, because this is called anyways just after booting...
        strip.setPixelColor( 0, 0, 0, 0, 0);
        strip.show();
        //Serial.println("III");
        //debug
        //delay(2000);
        //Serial.println("IIIa");
        //
        delay(500);
        for( int index = 0; index < 255; index += 2 ){
            //Serial.print("index: ");
            //Serial.println(index);
            strip.setPixelColor( 0, 0, index,  0, 0);
            //Serial.println("o");
            strip.show();
            //Serial.println("oo");
            analogWrite( UV_LED, index );
            //Serial.println("ooo");
            delay(10);
            //Serial.println("oooo");
        }
        //Serial.println("IV");
        for( int index = 255; index >= 0; index -= 2 ){
            strip.setPixelColor( 0, 0, index, 0, 0);
            //Serial.print("index: ");
            //Serial.println(index);
            strip.show();
            analogWrite(UV_LED, index);
            delay(10);
        }
        //Serial.println("IVa");
        delay(300);
        for( int index = 0; index < 255; index += 2 ){
            strip.setPixelColor( 0, 0, index,  0, 0);
            //Serial.print("index: ");
            //Serial.println(index);
            strip.show();
            analogWrite(UV_LED, index);
            delay(10);
        }
        //Serial.println("IVaa");
        for( int index = 255; index >= 0; index -= 2 ){
            strip.setPixelColor( 0, 0, index, 0, 0);
            //Serial.print("index: ");
            //Serial.println(index);
            strip.show();
            analogWrite(UV_LED, index);
            delay(10);
        }
        //Serial.println("V");
        strip.setPixelColor( 0, 0, 0, 0, 0);
        strip.show();
        analogWrite(UV_LED, 0);
        delay(2000);
    }
    #if DEBUG
    Serial.println("leaving makeWakeUpSignal()");
    #endif
}

void makeWifiConnectionSignal(){
    //Serial.println("entering makeSuccesSignale()!!!!!!!!!!!!!");
    //delay(4000);
    if( getMakeSignals() ){
        strip.setPixelColor( 0, 255, 0, 0, 0);
        strip.show();
        delay(300);
        strip.setPixelColor( 0, 0, 0, 0, 0);
        strip.show();
        delay(300);
        strip.setPixelColor( 0, 255, 0, 0, 0);
        strip.show();
        delay(300);
        strip.setPixelColor( 0, 0, 0, 0, 0);
        strip.show();
        delay(300);
        strip.setPixelColor( 0, 255, 0, 0, 0);
        strip.show();
        delay(300);
        strip.setPixelColor( 0, 0, 0, 0, 0);
        strip.show();
        delay(2000);
    }
    //Serial.println("leaving makeSuccesSignale()");
}

void makeServerConnectedSignale(){
    //Serial.println("entering makeSuccesSignale()");
    if( getMakeSignals() ){
        delay(500);
        for( int times = 0; times < 3; times++ ){
            switch ( times ){
                case 0:
                    strip.setPixelColor( 0, 0, 255, 0 );
                    break;
                case 1:
                    strip.setPixelColor( 0, 255, 0, 0 );
                    break;
                case 2:
                    strip.setPixelColor( 0, 0, 0, 255 );
                    break;
            }
            strip.show();
            delay(200);
            strip.setPixelColor( 0, 0, 0, 0 );
            colorSet noColor;
            pixels[0].currentColor = noColor;
            delay(50);
        }
        strip.setPixelColor( 0, 0, 0, 0, 0 );
        strip.show();
        delay(800);
    }
    //Serial.println("leaving makeSuccesSignale()");
}

void makeSignalForDeepSleep(){
    Serial.println("entering makeSignalForDeepSleep()");
    //for( int times = 0; times < 2; times++ ){
    for( int value = 255; value >= 0; value -= 2 ){
        if( value < 0 ){
            value = 0;
        }
        if( getMakeSignals() ){
            strip.setPixelColor( 0, value, value, value );  //                                                                                                                                                                                                                                                                         |
            strip.show();
        }
        delay(20);
    }
    if( getMakeSignals() ){
        strip.setPixelColor( 0, 0, 0, 0 );  //                                                                                                                                                                                                                                                                         |
        strip.show();
    }
    //}
    delay(1000);
    Serial.println("leaving makeSignalForDeepSleep()");
}

void makeSignalForTurningWifiOff(){
  
    // ok, this is a bit strange... it would be much nicer code to have this, but for some very strange reason, the client can't re-connect to the wifi, if getMakeSignal() returns false, and the hole block gets skipped. so this is why we are going to all this for-loops and only write the neo-pixels if getMakeSignals() returns true. it's ugly, but i don't know how i can make this nicer...
    //                                                                    |                                                                                                                                                                                                                                                              |
    Serial.println("entering makeSignalForTurningWifiOff()");//           |                                                                                                                                                                                                                                                              |
    //if( getMakeSignals() ){                       <----------------------                                                                                                                                                                                                                                                              |
    for( int times = 0; times < 3; times++ ){         //                                                                                                                                                                                                                                                                                 |
        for( int value = 255; value >= 0; value -= 2 ){ //                                                                                                                                                                                                                                                                               |
            if( value < 0 ){       //                                                                                                                                                                                                                                                                                                    |
                value = 0;          //                                                                                                                                                                                                                                                                                                   |
            }                       //                                                                                                                                                                                                                                                                                                   |
            if( getMakeSignals() ){ //     <---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
                strip.setPixelColor( 0, value, 0, 0, 0);  //                                                                                                                                                                                                                                                                             |
                strip.show(); //                                                                                                                                                                                                                                                                                                         |
            }   //                                                                                                                                                                                                                                                                                                                       |
            delay(10);  //                                                                                                                                                                                                                                                                                                               |
        }  //                                                                                                                                                                                                                                                                                                                            |
        if( getMakeSignals() ){ //     <-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
            strip.setPixelColor( 0, 0, 0, 0, 0);
            strip.show();
        }
    }
    delay(2000);
    //}
    Serial.println("leaving makeSignalForTurningWifiOff()");
}

void makeSignalForTurningWifiOn(){
    // please look at makeSignalForTurningWifiOff() to understand the uglyness of this function...
    
    Serial.println("entering makeSignalForTurningWifiOn()");
    //if( getMakeSignals() ){
    for( int times = 0; times < 3; times++ ){
        for( int value = 0; value <= 255; value += 2 ){
            if( value > 255 ){
              value = 255;
            }
            if( getMakeSignals() ){
              strip.setPixelColor( 0, value, 0, 0, 0);
              strip.show();
            }
            delay(10);
        }
        if( getMakeSignals() ){
            strip.setPixelColor( 0, 0, 0, 0, 0);
            strip.show();
        }
    }
    delay(2000);
    //}
    Serial.println("leaving makeSignalForTurningWifiOn()");
}

void handlePing(){
    #if DEBUG
    //Serial.println("ping received!");
    #endif
    #if USE_PING_TIME_TO_CHECK_DISCONNECTION
    timeLastPingReceived = millis();
    #endif
}

colorSet extractColorSet( String in_colorSetString ){
  
    // for example: "015,123,001,042"   (red, green, blue, uv)
  
    #if DEBUG
    //Serial.print("entering extractColorSet() ... in_colorSetString: ");
    //Serial.println( in_colorSetString );
    #endif
  
    colorSet output;
    bool redFound = false;
    bool greenFound = false;
    bool blueFound = false;
    bool uvFound = false;
    String redString = "";
    String greenString = "";
    String blueString = "";
    String uvString = "";
    for( int index = 0; index < in_colorSetString.length(); index++ ){
        char symbol = in_colorSetString.charAt( index );
        if( redFound == false && greenFound == false && blueFound == false  ){
            if( symbol == ',' ){
                redFound = true;
                index++;
                symbol = in_colorSetString.charAt( index );
            }else{
                redString += symbol;
            }
        }
        if( redFound == true && greenFound == false && blueFound == false ){
            if( symbol == ',' ){
                greenFound = true;
                index++;
                symbol = in_colorSetString.charAt( index );
            }else{
               greenString += symbol;
            }
        }
        if( redFound == true && greenFound == true && blueFound == false ){
            if( symbol == ',' ){
                blueFound = true;
                index++;
                symbol = in_colorSetString.charAt( index );
            }else{
                blueString += symbol;
            }
        }
        if( redFound == true && greenFound == true && blueFound == true ){
            if( symbol == ',' ){
                uvFound = true;
                index++;
                symbol = in_colorSetString.charAt( index );
            }else{
                uvString += symbol;
            }
        }  
    }
  
    #if DEBUG
    //Serial.print("redString: ");
    //Serial.println(redString);
    //Serial.print("greenString: ");
    //Serial.println(greenString);
    //Serial.print("blueString: ");
    //Serial.println(blueString);
    //Serial.print("uvString: ");
    //Serial.println(uvString);
    #endif
  
    if( redString == "r" ){
        output.red = random( 0, 256 );  // a value between 0 and 256-1, means: the highest number will be 255
    }else{
        output.red = redString.toInt();
    }
    if( greenString == "r" ){
        output.green = random( 0, 256 );
    }else{
        output.green = greenString.toInt();
    }
    if( blueString == "r" ){
        output.blue = random( 0, 256 );
    }else{
        output.blue = blueString.toInt();
    }
    if( uvString == "r" ){
        output.uv = random( 0, 256 );
    }else{
        output.uv = uvString.toInt();
    }   
    
    #if DEBUG
    Serial.print("output.red: ");
    Serial.println(output.red);
    Serial.print("output.green: ");
    Serial.println(output.green);
    Serial.print("output.blue: ");
    Serial.println(output.blue);
    Serial.print("output.uv: ");
    Serial.println(output.uv);
    Serial.println("leaving extractColorSet()");
    #endif
    
    return output;
}

void turnOffAllPixels(){
    //int chainLength = getChainLength();
    for( int pixelIndex = 0; pixelIndex < chainLength; pixelIndex++ ){
        strip.setPixelColor( pixelIndex, 0, 0, 0, 0 );
        //strip.show();
    }
    strip.show();
}

void updatePixel( int in_pixelNumber ){  //watch out. strip.show() must be called after updatePixel()-call !!!!elf

    #if DEBUG
    Serial.println("");
    Serial.print("entering updatePixel(): ");
    Serial.print("colorStateArray[");
    Serial.print(in_pixelNumber);
    Serial.print("].destColor:\t");
    printColorSet( pixels[in_pixelNumber].fades[0].destinationColor );
    Serial.print("colorStateArray[in_pixelNumber].currentColor: \t");
    printColorSet( pixels[in_pixelNumber].currentColor );
    #endif

    //bool somethingHasChanged = false;
  
    if( pixels[in_pixelNumber].fades[0].steps != 1 ){ //to save performance we trade the cases with a 'real' fade different then the cases where we just change the color at once...
        #if DEBUG
        Serial.println("pixels[in_pixelNumber].fades[0].steps != 1");
        #endif

        pixels[in_pixelNumber].fades[0].currentStepOfCurrentFade++;

        //Serial.print("pixels[in_pixelNumber].fades[0].currentStepOfCurrentFade: ");
        //Serial.println(pixels[in_pixelNumber].fades[0].currentStepOfCurrentFade);
        
        pixels[in_pixelNumber].currentColor.red = pixels[in_pixelNumber].fades[0].startColor.red + pixels[in_pixelNumber].fades[0].fadeDelta.red * pixels[in_pixelNumber].fades[0].currentStepOfCurrentFade;
        pixels[in_pixelNumber].currentColor.green = pixels[in_pixelNumber].fades[0].startColor.green + pixels[in_pixelNumber].fades[0].fadeDelta.green * pixels[in_pixelNumber].fades[0].currentStepOfCurrentFade;
        pixels[in_pixelNumber].currentColor.blue = pixels[in_pixelNumber].fades[0].startColor.blue + pixels[in_pixelNumber].fades[0].fadeDelta.blue * pixels[in_pixelNumber].fades[0].currentStepOfCurrentFade;
        pixels[in_pixelNumber].currentColor.uv = pixels[in_pixelNumber].fades[0].startColor.uv + pixels[in_pixelNumber].fades[0].fadeDelta.uv * pixels[in_pixelNumber].fades[0].currentStepOfCurrentFade;

        //Serial.print("pixels[in_pixelNumber].currentColor: ");
        //printColorSet(pixels[in_pixelNumber].currentColor);

    
        #if DEBUG
        Serial.print("pixels[in_pixelNumber].fades[0].fadeDelta.red: ");
        Serial.println(pixels[in_pixelNumber].fades[0].fadeDelta.red);
        Serial.print("pixels[in_pixelNumber].fades[0].fadeDelta.green: ");
        Serial.println(pixels[in_pixelNumber].fades[0].fadeDelta.green);
        Serial.print("pixels[in_pixelNumber].fades[0].fadeDelta.blue: ");
        Serial.println(pixels[in_pixelNumber].fades[0].fadeDelta.blue);
        Serial.print("pixels[in_pixelNumber].fades[0].fadeDelta.uv: ");
        Serial.println(pixels[in_pixelNumber].fades[0].fadeDelta.uv);
        #endif    
    
        if( pixels[in_pixelNumber].fades[0].fadeDirection.red == up ){
            if( pixels[in_pixelNumber].currentColor.red > pixels[in_pixelNumber].fades[0].destinationColor.red ){
                pixels[in_pixelNumber].currentColor.red = pixels[in_pixelNumber].fades[0].destinationColor.red;
            }
        }else if( pixels[in_pixelNumber].fades[0].fadeDirection.red == down ){
            if( pixels[in_pixelNumber].currentColor.red < pixels[in_pixelNumber].fades[0].destinationColor.red ){
                pixels[in_pixelNumber].currentColor.red = pixels[in_pixelNumber].fades[0].destinationColor.red;
            }
        }
        if( pixels[in_pixelNumber].fades[0].fadeDirection.green == up ){
            if( pixels[in_pixelNumber].currentColor.green > pixels[in_pixelNumber].fades[0].destinationColor.green ){
                pixels[in_pixelNumber].currentColor.green = pixels[in_pixelNumber].fades[0].destinationColor.green;
            }
        }else if( pixels[in_pixelNumber].fades[0].fadeDirection.green == down ){
            if( pixels[in_pixelNumber].currentColor.green < pixels[in_pixelNumber].fades[0].destinationColor.green ){
                pixels[in_pixelNumber].currentColor.green = pixels[in_pixelNumber].fades[0].destinationColor.green;
            }
        }
        if( pixels[in_pixelNumber].fades[0].fadeDirection.blue == up ){
            if( pixels[in_pixelNumber].currentColor.blue > pixels[in_pixelNumber].fades[0].destinationColor.blue ){
                pixels[in_pixelNumber].currentColor.blue = pixels[in_pixelNumber].fades[0].destinationColor.blue;
            }
        }else if( pixels[in_pixelNumber].fades[0].fadeDirection.blue == down ){
            if( pixels[in_pixelNumber].currentColor.blue < pixels[in_pixelNumber].fades[0].destinationColor.blue ){
                pixels[in_pixelNumber].currentColor.blue = pixels[in_pixelNumber].fades[0].destinationColor.blue;
            }
        }
        if( pixels[in_pixelNumber].fades[0].fadeDirection.uv == up ){
            if( pixels[in_pixelNumber].currentColor.uv > pixels[in_pixelNumber].fades[0].destinationColor.uv ){
                pixels[in_pixelNumber].currentColor.uv = pixels[in_pixelNumber].fades[0].destinationColor.uv;
            }
        }else if( pixels[in_pixelNumber].fades[0].fadeDirection.uv == down ){
            if( pixels[in_pixelNumber].currentColor.uv < pixels[in_pixelNumber].fades[0].destinationColor.uv ){
                pixels[in_pixelNumber].currentColor.uv = pixels[in_pixelNumber].fades[0].destinationColor.uv;
            }
        }
        //somethinHasChanged = true;
  
    }else{ // color-change at once (in 1 step)
        #if DEBUG
        Serial.println("color-change in 1 step!!!");
        #endif
    
        //Serial.print("current fade destinationColor: ");
        //printColorSet( pixels[in_pixelNumber].fades[0].destinationColor );
        
        pixels[in_pixelNumber].fades[0].currentStepOfCurrentFade++;
        
        pixels[in_pixelNumber].currentColor.red = pixels[in_pixelNumber].fades[0].destinationColor.red;
        pixels[in_pixelNumber].currentColor.green = pixels[in_pixelNumber].fades[0].destinationColor.green;
        pixels[in_pixelNumber].currentColor.blue = pixels[in_pixelNumber].fades[0].destinationColor.blue;
        pixels[in_pixelNumber].currentColor.uv = pixels[in_pixelNumber].fades[0].destinationColor.uv;
        //somethinHasChanged = true;
    }
  
    #if DEBUG
    Serial.print("calling setPixelColor(): in_pixelNumber: ");
    Serial.print(in_pixelNumber);
    Serial.print(" ... green: ");
    Serial.print(pixels[in_pixelNumber].currentColor.green);
    Serial.print(" ... red: ");
    Serial.print(pixels[in_pixelNumber].currentColor.red);
    Serial.print(" ... blue: ");
    Serial.print(pixels[in_pixelNumber].currentColor.blue);
    Serial.print(" ... uv: ");
    Serial.println(pixels[in_pixelNumber].currentColor.uv);
    #endif


    if( in_pixelNumber == 0 ){  // the through-hole-neo-pixels need a different color then the stripes... see this: https://forums.adafruit.com/viewtopic.php?f=47&t=89264
        strip.setPixelColor( in_pixelNumber, pixels[in_pixelNumber].currentColor.green, pixels[in_pixelNumber].currentColor.red, pixels[in_pixelNumber].currentColor.blue );
    }else{
        strip.setPixelColor( in_pixelNumber, pixels[in_pixelNumber].currentColor.red, pixels[in_pixelNumber].currentColor.green, pixels[in_pixelNumber].currentColor.blue );
    }
    //strip.show();   // to safe performance, we do that in updateAllPixels()
  
  
    // and now the uv-part:
    analogWrite(UV_LED, pixels[in_pixelNumber].currentColor.uv);
    // well... that was easy


    //Serial.println("leaving updatePixel()");

}

void calculateCurrentFadeForPixel( int in_pixelNumber ){
    #if DEBUG
    Serial.print("entering calculateCurrentFadeForPixel() ... in_pixelNumber: ");
    Serial.println(in_pixelNumber);
    #endif
  
    if( pixels[in_pixelNumber].fades.size() > 0 ){
        #if DEBUG
        Serial.print("pixels[");
        Serial.print(in_pixelNumber);
        Serial.print("].fades.size(): ");
        Serial.println(pixels[in_pixelNumber].fades.size());
        #endif
            
        if( pixels[in_pixelNumber].fades[0].destinationColor.red > pixels[in_pixelNumber].currentColor.red ){
            //Serial.println("pixels[in_pixelNumber].fades[0].destinationColor.red > pixels[in_pixelNumber].currentColor.red");
            pixels[in_pixelNumber].fades[0].fadeDirection.red = up;
        }else{
            //Serial.println("pixels[in_pixelNumber].fades[0].destinationColor.red =< pixels[in_pixelNumber].currentColor.red");
            pixels[in_pixelNumber].fades[0].fadeDirection.red = down;
        }
    
        if( pixels[in_pixelNumber].fades[0].destinationColor.green > pixels[in_pixelNumber].currentColor.green ){
            //Serial.println("pixels[in_pixelNumber].fades[0].destinationColor.green > pixels[in_pixelNumber].currentColor.green");
            pixels[in_pixelNumber].fades[0].fadeDirection.green = up;
        }else{
            //Serial.println("pixels[in_pixelNumber].fades[0].destinationColor.green =< pixels[in_pixelNumber].currentColor.green");
            pixels[in_pixelNumber].fades[0].fadeDirection.green = down;
        }
    
        if( pixels[in_pixelNumber].fades[0].destinationColor.blue > pixels[in_pixelNumber].currentColor.blue ){
            //Serial.println("pixels[in_pixelNumber].fades[0].destinationColor.blue > pixels[in_pixelNumber].currentColor.blue");
            pixels[in_pixelNumber].fades[0].fadeDirection.blue = up;
        }else{
            //Serial.println("pixels[in_pixelNumber].fades[0].destinationColor.blue =< pixels[in_pixelNumber].currentColor.blue");
            pixels[in_pixelNumber].fades[0].fadeDirection.blue = down;
        }
    
        if( pixels[in_pixelNumber].fades[0].destinationColor.uv > pixels[in_pixelNumber].currentColor.uv ){
            //Serial.println("pixels[in_pixelNumber].fades[0].destinationColor.blue > pixels[in_pixelNumber].currentColor.blue");
            pixels[in_pixelNumber].fades[0].fadeDirection.uv = up;
        }else{
            //Serial.println("pixels[in_pixelNumber].fades[0].destinationColor.blue =< pixels[in_pixelNumber].currentColor.blue");
            pixels[in_pixelNumber].fades[0].fadeDirection.uv = down;
        }
    
        #if DEBUG
        Serial.print("pixels[in_pixelNumber].fades[0].steps: ");
        Serial.println(pixels[in_pixelNumber].fades[0].steps);
        #endif

        /*
        pixels[ in_pixelNumber ].fades[0].fadeDelta.red = (int)( ((float)pixels[in_pixelNumber].fades[0].destinationColor.red - (float)pixels[in_pixelNumber].currentColor.red) / (float)pixels[in_pixelNumber].fades[0].steps + 0.5 );
        pixels[ in_pixelNumber ].fades[0].fadeDelta.green = (int)( ((float)pixels[in_pixelNumber].fades[0].destinationColor.green - (float)pixels[in_pixelNumber].currentColor.green) / (float)pixels[in_pixelNumber].fades[0].steps + 0.5 );
        pixels[ in_pixelNumber ].fades[0].fadeDelta.blue = (int)( ((float)pixels[in_pixelNumber].fades[0].destinationColor.blue - (float)pixels[in_pixelNumber].currentColor.blue) / (float)pixels[in_pixelNumber].fades[0].steps + 0.5 );
        pixels[ in_pixelNumber ].fades[0].fadeDelta.uv = (int)( ((float)pixels[in_pixelNumber].fades[0].destinationColor.uv - (float)pixels[in_pixelNumber].currentColor.uv) / (float)pixels[in_pixelNumber].fades[0].steps + 0.5 );
        */

        pixels[ in_pixelNumber ].fades[0].fadeDelta.red = ((float)pixels[in_pixelNumber].fades[0].destinationColor.red - (float)pixels[in_pixelNumber].currentColor.red) / (float)pixels[in_pixelNumber].fades[0].steps;
        pixels[ in_pixelNumber ].fades[0].fadeDelta.green = ((float)pixels[in_pixelNumber].fades[0].destinationColor.green - (float)pixels[in_pixelNumber].currentColor.green) / (float)pixels[in_pixelNumber].fades[0].steps;
        pixels[ in_pixelNumber ].fades[0].fadeDelta.blue = ((float)pixels[in_pixelNumber].fades[0].destinationColor.blue - (float)pixels[in_pixelNumber].currentColor.blue) / (float)pixels[in_pixelNumber].fades[0].steps;
        pixels[ in_pixelNumber ].fades[0].fadeDelta.uv = ((float)pixels[in_pixelNumber].fades[0].destinationColor.uv - (float)pixels[in_pixelNumber].currentColor.uv) / (float)pixels[in_pixelNumber].fades[0].steps;

        #if DEBUG
        Serial.print("pixels[ in_pixelNumber ].fades[0].fadeDelta.red: ");
        Serial.println( pixels[ in_pixelNumber ].fades[0].fadeDelta.red );
        Serial.print("pixels[ in_pixelNumber ].fades[0].fadeDelta.green: ");
        Serial.println( pixels[ in_pixelNumber ].fades[0].fadeDelta.green );
        Serial.print("pixels[ in_pixelNumber ].fades[0].fadeDelta.blue: ");
        Serial.println( pixels[ in_pixelNumber ].fades[0].fadeDelta.blue );
        Serial.print("pixels[ in_pixelNumber ].fades[0].fadeDelta.uv: ");
        Serial.println( pixels[ in_pixelNumber ].fades[0].fadeDelta.uv );
        #endif

        pixels[ in_pixelNumber ].fades[0].startColor.red = pixels[ in_pixelNumber ].currentColor.red;
        pixels[ in_pixelNumber ].fades[0].startColor.green = pixels[ in_pixelNumber ].currentColor.green;
        pixels[ in_pixelNumber ].fades[0].startColor.blue = pixels[ in_pixelNumber ].currentColor.blue;
        pixels[ in_pixelNumber ].fades[0].startColor.uv = pixels[ in_pixelNumber ].currentColor.uv;
    
    }
  
    #if DEBUG
    Serial.println("leaving calculateCurrentFadeForPixel()");
    #endif
}

bool isColorSetEqual( colorSet in_first, colorSet in_second ){
    #if DEBUG
    //Serial.println("entering isColorSetEqual()");
    //Serial.println("in_first: ");
    //printColorSet( in_first );
    //Serial.println("in_second: ");
    //printColorSet( in_second );
    #endif
  
    bool output = true;
    if( in_first.red != in_second.red || in_first.green != in_second.green || in_first.blue != in_second.blue || in_first.uv != in_second.uv ){
        output = false;
    }
  
    #if DEBUG
    //Serial.print("leaving isColorSetEqual() ... returning: ");
    //Serial.println(output);
    #endif
    return output;
}

void updateAllPixels(){
    #if DEBUG
    //Serial.println("entering updateAllPixels()");
    #endif
    delay(50); //this litte delay will make the fade between the colors a bit smoother!
    bool somethingHasChanged = false;
    //int chainLength = getChainLength();
    for( int index = 0; index < chainLength; index++ ){
        if( pixels[index].fades.size() > 0 ){
            #if DEBUG
            Serial.print("pixel[");
            Serial.print(index);
            Serial.print("].fades.size(): ");
            Serial.println(pixels[index].fades.size());
            #endif
            colorSet currentColor = pixels[index].currentColor;
            colorSet destinationColor = pixels[index].fades[0].destinationColor;
            if( pixels[index].fades[0].currentStepOfCurrentFade != pixels[index].fades[0].steps ){
            //if( isColorSetEqual(currentColor, destinationColor) == false ){
                #if DEBUG
                Serial.println("pixels[index].fades[0].currentStepOfCurrentFade != pixels[index].fades[0].steps");
                #endif
                updatePixel( index );
                somethingHasChanged = true;
                //delay(50); //this litte delay will make the fade between the colors a bit smoother!
            }else{
                // current fade is done...
                #if DEBUG
                Serial.println("pixelGotChanged == false");
                #endif
        
                #if DEBUG
                Serial.print("fadeInProcess == true ... ");
                Serial.print("current fade finnished ... ");
                Serial.println("setting fadeInProcess = false");
                delay(1000);
                Serial.println("removing fade[0] from vector!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                delay(1000);
                #endif
        
                pixels[index].timeCurrentStateShouldSustainForAtLeast = pixels[index].fades[0].sustain;
                //Serial.println("removing fades[0]!!!!!!!!!!!!!!!!!!!!!!!!");
                pixels[index].fades.erase( pixels[index].fades.begin() );
                calculateCurrentFadeForPixel( index );
                pixels[index].timeLastFadeHasBeenFinished = millis();
                //currentStepOfCurrentFade = 0;
            }
        }
    }
    if( somethingHasChanged ){
        strip.show(); //to safe performance, we do that here, not in updatePixel()
    }
    #if DEBUG
    //Serial.println("leaving updateAllPixels()");
    //delay(2000);
    #endif
}

void printColorSet( colorSet in_colorSet ){
    //Serial.println("");
    Serial.print("red: ");
    Serial.print(in_colorSet.red);
    Serial.print(" ... green: ");
    Serial.print(in_colorSet.green);
    Serial.print(" ... blue: ");
    Serial.print(in_colorSet.blue);
    Serial.print(" ... uv: ");
    Serial.println(in_colorSet.uv);
}

void breakFadesAndDeleteFutureFades(){
    #if DEBUG
    Serial.println("entering breakCurrentFade()");
    #endif
    long currentTime = millis();
    //int chainLength = getChainLength();
    for( int index = 0; index < chainLength; index++ ){
        pixels[index].fades.clear();
        pixels[index].timeCurrentStateShouldSustainForAtLeast = 0;
        pixels[index].timeLastFadeHasBeenFinished = currentTime;
        //!pixels[index].fadeInProcess = false;
    }
    #if DEBUG
    Serial.println("leaving breakCurrentFade()");
    #endif
}

void addFadeForPixel( int in_pixelNumber, colorSet in_destinationColor, int in_steps, int in_sustain ){
    #if DEBUG
    Serial.println("entering addFadeForPixel()");
    #endif
  
    fade thisFade;
    thisFade.steps = in_steps;
    thisFade.sustain = in_sustain;
    // thisFade.fadeDelta     // we cant set this here, because for that, we need to know the currentColor
    // thisFade.fadeDirection // same with this
    thisFade.destinationColor = in_destinationColor;
    pixels[ in_pixelNumber ].fades.push_back( thisFade );
  
    #if DEBUG
    Serial.println("leaving addFadeForPixel()");
    #endif
}

void addFadeForAllPixels( fade in_fade ){
  #if DEBUG
  Serial.println("entering addFadeForAllPixels()");
  #endif
  //int chainLength = getChainLength();
  for( int index = 0; index < chainLength; index++ ){
      addFadeForPixel( index, in_fade.destinationColor, in_fade.steps, in_fade.sustain );
  }
  #if DEBUG
  Serial.println("leaving addFadeForAllPixels()");
  #endif
}

fade parseFade( String in_fadeString ){
    fade output;
  
    // one fade looks like this:
    // 255,0,0,0-1-300
    // ^^^^^^^ color
    //         ^ uv-value
    //           ^ fade in steps
    //             ^^^ sustain for milliseconds
  
    String colorSetString = "";
    bool colorSetFound = false;
    String stepsString = "";
    bool stepsFound = false;
    String sustainString = "";
    //bool sustainFound = false;
    char symbol;
    for( int index = 0; index < in_fadeString.length(); index++ ){
        symbol = in_fadeString.charAt( index  );
        if( colorSetFound == false && stepsFound == false /* && sustainFound == false */ ){
            if( symbol != '-' ){
                colorSetString += symbol;
            }else{
                colorSetFound = true;
                index++;
                symbol = in_fadeString.charAt( index );
            }
        }
        if( colorSetFound && stepsFound == false /* && sustainFound == false */ ){
            if( symbol != '-' ){
              stepsString += symbol;
            }else{
              stepsFound = true;
              index++;
              symbol = in_fadeString.charAt( index );
          }
        }
        if( colorSetFound && stepsFound /* && sustainFound == false */ ){
            sustainString += symbol;
        }
    }
    output.destinationColor = extractColorSet( colorSetString );
    output.steps = stepsString.toInt();

    //!!! we don't need this any more, since we made sure, to send at least 1 as value from server to client. (safing a bit of performance)
    //if( output.steps == 0 ){    // just to make sure, we don't devide by zero. but we should make sure, to avoid sending zero to the client at the first place...
      //Serial.println("parsed output.steps as zero!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      //Serial.println("setting it to 1 instead!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      //output.steps++;
    //}
    
    output.sustain = sustainString.toInt();
    

    //debug
    //Serial.print("output.destinationColor: ");
    //printColorSet( output.destinationColor );
    //Serial.print("output.steps: ");
    //Serial.println( output.steps );
    //Serial.print("output.sustain: ");
    //Serial.println(output.sustain);
    //
    
    //Serial.println("leaving parseFade()");
    return output;
}

void processFadeSeriesForAllPixels( String in_command ){
    #if DEBUG
    Serial.print("entering processFadeSeriesForAllPixels() .. in_command: ");
    Serial.println(in_command);
    #endif
  
    // an fadeSeries looks like this:
    // 255,0,0,0-1-300_0,0,0,0-1-1337_
    // it's a series of fades. (no way!!)
  
    String singleFadeString = "";
    char symbol;
    //fadeStack.clear();
    for( int index = 0; index < in_command.length(); index++ ){
        symbol = in_command.charAt( index );
        if( symbol != '_' ){
            singleFadeString += symbol;
        }else{
            #if DEBUG
            Serial.print("pushing one fade on fadeStack. singeFadeString: ");
            Serial.println(singleFadeString);
            #endif
            
            addFadeForAllPixels( parseFade( singleFadeString ) );
            singleFadeString = "";
        }
    }
    calculateCurrentFadeForPixel( 0 );
  
    #if DEBUG
    Serial.println("leaving processFadeSeriesForAllPixels()");
    #endif
}

void setTriggerThreshold( int in_newValue ){
    Serial.print("setTriggerThreshold(): setting trigger-threshold to new value: ");
    Serial.println( in_newValue );
    analogWrite( TRIGGER_THRESHOLD_OUT, in_newValue );
}

void processCommand(String in_command){
  
    #if DEBUG
    //Serial.print("entering processCommand() ... in_command: ");
    //Serial.println( in_command );
    #endif
  
    String commandType = "";
    bool commandTypeComplete = false;
    String commandContent = "";
  
    //#if DEBUG
    //Serial.print("in_command.length: ");
    //Serial.println(  in_command.length() );
    //#endif
    for( int index = 0; index < in_command.length(); index++ ){
        char symbol = in_command.charAt( index );
        if( symbol == ';' ){
            commandTypeComplete = true;
            index++;
            symbol = in_command.charAt( index );
        }
    
        //#if DEBUG
        //Serial.print("symbol: ");
        //Serial.println(symbol);
        //#endif
    
        if( commandTypeComplete == false ){
            commandType += symbol;
        }else{
            commandContent += symbol;
        }
    }
  
    //#if DEBUG
    //Serial.print("commandType: ");
    //Serial.println(commandType);
    //Serial.print("commandContent: ");
    //Serial.println(commandContent);
    //#endif
    if( commandType == "ping" ){
        //Serial.println(":");
        handlePing();
        return;
    }  
    //! implemented command:
  
    if( commandType == "fadeSeries4AllPixels" ){
        breakFadesAndDeleteFutureFades();
        //Serial.print("commandContent: ");
        //Serial.println( commandContent );
        processFadeSeriesForAllPixels( commandContent );
        /*
          }else if( commandType == "sleepSeconds" ){
            Serial.print("sleeping for ");
            Serial.print( commandContent );
            Serial.println(" Seconds now! Good Night, Sir!");
            WiFi.forceSleepBegin();
            delay( commandContent.toInt()*1000 );
            WiFi.forceSleepWake();
            Serial.println("back up...");
            //justWokeUp = true;
        */
    }else if( commandType == "makeSignals" ){
        Serial.println("commandType == makeSignals");
        Serial.print("commandContent: ");
        Serial.println(commandContent);
        if( commandContent == "true" ){
            setMakeSignals( true );
        }else if( commandContent == "false" ){
            setMakeSignals( false );
        }else{
            Serial.print("unknown command ... in_command: ");
            Serial.println( in_command );
            delay(5000);
        }
    }else if( commandType == "deepSleepOnce" ){
        Serial.println("received deepSleepOnce");
        doDeepSleepForSeconds( commandContent.toInt() );
    }else if( commandType == "deepSleepInLoopOn" ){
        Serial.println("received deepSleepInLoopOn");
        setDeepSleepInLoopForSeconds( commandContent.toInt() );
        setDoDeepSleepInLoop( true );
        doDeepSleepForSeconds( getDeepSleepInLoopForSeconds() );
    }else if( commandType == "deepSleepInLoopOff" ){
        Serial.println("received deepSleepInLoopOff");
        setDoDeepSleepInLoop( false );
    }else if( commandType == "continueDeepSleepInLoop" ){
        Serial.println("received continueDeepSleepInLoop");
        doDeepSleepForSeconds( getDeepSleepInLoopForSeconds() );
    }else if( commandType == "wifiOffForSeconds" ){
        Serial.println("received wifiForcedSleepForSeconds");
        doWifiForcedSleepForSeconds( commandContent.toInt() );
    }else if( commandType == "wifiForcedSleepLoopOn" ){
        //#if DEBUG
        Serial.println("received wifiForcedSleepLoopOn()");
        //#endif
        setDoWifiForcedSleepInLoop(true);
        Serial.print("commandContent.toInt(): ");
        Serial.println(commandContent.toInt());
        setWifiForcedSleepInLoopForSeconds( commandContent.toInt() );
        //Serial.print("getWifiForcedSleepInLoopForSeconds(): ");
        //Serial.println(getWifiForcedSleepInLoopForSeconds());
        doWifiForcedSleepForSeconds( getWifiForcedSleepInLoopForSeconds() );
    }else if( commandType == "wifiForcedSleepLoopOff" ){
        //#if DEBUG
        Serial.println("received wifiForcedSleepLoopOff()");
        //#endif
        setDoWifiForcedSleepInLoop( false );
    }else if( commandType == "setChainLength" ){
        Serial.println("received setChainLength ... commandContent: " + commandContent);
        chainLength = commandContent.toInt();
        buildPixelVector();
        writeServer( "@chainLength;" + commandContent + "#" );
        //setChainLength( commandContent.toInt() );
        //reset();
    }else if( commandType == "continueWifiForcedSleepInLoop" ){
        Serial.println("received continueWifiForcedSleepInLoop");
        doWifiForcedSleepForSeconds( getWifiForcedSleepInLoopForSeconds() );
    }else if( commandType == "setTriggerThreshold" ){
        setTriggerThreshold( commandContent.toInt() );
    }else if( commandType == "reset" ){
        Serial.println("calling reset() because received command to do so...");
        reset();
    }else if( commandType == "setDoFadesOnTrigger" ){
        if( commandContent == "true" ){
            doFadesOnTrigger = true;
        }else if( commandContent == "false" ){
            doFadesOnTrigger = false;
        }else{
            Serial.println("bullshit");
        }
    }else if( commandType == "sendTriggerSignalsToServer" ){
        if( commandContent == "true" ){
            sendTriggerSignalsToServer = true;
        }else if( commandContent == "false" ){
            sendTriggerSignalsToServer = false;
        }else{
            Serial.println("bullshit");
        }
    }else{
        Serial.print("unknown command ... in_command: ");
        Serial.println( in_command );
        delay(5000);
    }
}

void writeServer( String in_toSendMessage ){

    #if DEBUG
    Serial.print("trying to write client ... in_toSendMessage: ");
    Serial.print(in_toSendMessage);
    Serial.print(" ... ");
    #endif
  
    int bytesWritten = client.print( in_toSendMessage );
  
    #if DEBUG
    Serial.print("done writing. bytesWritten: ");
    Serial.println(bytesWritten);
    #endif
  
    #if USE_PING_TIME_TO_CHECK_DISCONNECTION == false
    if( bytesWritten == 0 ){
        delay(1000);
        Serial.print("connection to server seems to be lost ");
        turnOffAllPixels();
        connectedToServer = false;
        delay(1000);
    }
    #endif

}

void updateSignalStrength(){
    if( SignalStrengthUpdateCounter == 0 ){
        //SignalStrengthUpdateCounter = 0;
        //Serial.println(".");
        wifiSignalStrength = String( WiFi.RSSI() );
    }
    SignalStrengthUpdateCounter++;
    SignalStrengthUpdateCounter %= 10;
}

void sendPingToServer(){
    //Serial.println("sending ping to Server");
    //Serial.println(".");
    String toSendString = "@ping;" + wifiSignalStrength + "#";
    writeServer( toSendString );
    timeLastPingSent = millis();
}

void readFromSocketAndExecute(){
  
    char symbol = 0;
    do{
        //Serial.println("client.read()");
        symbol = client.read();
        //Serial.print("symbol: ");
        //Serial.println(symbol);
        //Serial.print("(int)symbol: ");
        //Serial.println((int)symbol);
        if( symbol != 255 ){
            messageBuffer += symbol;
        }
        if( symbol == '#' ){
            numberOfCompleteMessagesInMessgeBuffer++;
        }
    }while( (int)symbol != 255 );
      for( int index = 0; index < numberOfCompleteMessagesInMessgeBuffer; index++ ){
        //Serial.print("index: ");
        //Serial.println(index);
        String receivedCommand = "";
        bool complete = false;
        while( complete == false ){      
            symbol = messageBuffer.charAt( 0 );
            if( symbol == '#' ){
                complete = true;
            }
            messageBuffer.remove( 0, 1 );
            if( symbol != '#' && symbol != '@' ){
                receivedCommand += symbol;
            }
        }
        #if DEBUG
        Serial.print("calling processCommand() ... receivedCommand: ");
        Serial.println(receivedCommand);
        #endif
        processCommand( receivedCommand );
    }
    numberOfCompleteMessagesInMessgeBuffer = 0;
}

void sendSignal_0_signal( bool in_edge ){ // true == positive edge, false == negative edge
    String toSendMessage = "@button0;";
    if( in_edge == true ){
        toSendMessage += "positiveEdge";
    }else{
        toSendMessage += "negativeEdge";
    }
    toSendMessage += "#";     // e.g. @button0;positiveEdge#
    writeServer( toSendMessage );
}

void checkButton0(){
    //Serial.print("digitalRead( BUTTON_0 ): ");
    //Serial.println( digitalRead( BUTTON_0 ) );
    bool currentButton0_state = digitalRead( BUTTON_0 );
    if( currentButton0_state == true && button0_oldState == false ){
        unsigned long currentTime = millis();
        if( currentTime - button0_lastPressed >= BUTTON_TIMEOUT ){
            Serial.println("Button0 positive edge detected!!!!");
            button0_oldState = true;
            button0_lastPressed = currentTime;
            sendSignal_0_signal( true );
        }
    }else if( currentButton0_state == false && button0_oldState == true ){
        unsigned long currentTime = millis();
        if( currentTime - button0_lastPressed >= BUTTON_TIMEOUT ){
            Serial.println("Button0 negative edge detected!!!!");
            button0_oldState = false;
            button0_lastPressed = currentTime;
            sendSignal_0_signal( false );
        }
    }
}


void doFadesAfterTrigger(  ){
    Serial.println("entering doFadesAfterTrigger()");
    // missing
}

void sendTriggerSignal(  ){
    Serial.println("entering sendTriggerSignal()"); 
    String toSendMessage = "@trigger;foobar#";
    writeServer( toSendMessage );
}


void checkTriggerIn(){
    trigger_oldState = trigger_state;
    trigger_state = digitalRead( TRIGGER_INPUT );
    if( trigger_state == true ){
        if( trigger_oldState == false ){
            Serial.println("!!!!!!!!!!!!!!!!!!!!!!!!!! triggered !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            if( doFadesOnTrigger == true ){
                Serial.println("doFadesOnTrigger == true");
                doFadesAfterTrigger();
            }
            if( sendTriggerSignalsToServer == true ){
                Serial.println("sendTriggerSignal == true");
                sendTriggerSignal();
            }
        }
    }
}

void loop(){
  
    #if DEBUG
    delay(1000);
    #endif
  
    ownIP = WiFi.localIP();
    #if DEBUG
    //Serial.print( "WiFi.localIP(): " );
    //Serial.println( ownIP );
    #endif
  
    if( wifiIsOn == false ){
        Serial.println( "wifiIsOn == false" );
        long currentTime = millis();
        Serial.print("currentTime: ");
        Serial.println(currentTime);
        Serial.print("keepWifiOffAtLeastUntil: ");
        Serial.println(keepWifiOffAtLeastUntil);
        if( currentTime >= keepWifiOffAtLeastUntil ){
            Serial.println("millis() >= keepWifiOffAtLeastUntil ... waking up wifi!");
            wakeUpWifiForcedSleep();
        }else{
            Serial.println("to early to wake up wifi");
        }
    }else{
        #if DEBUG
        Serial.println("wifiIsOn == true");
        #endif
    }
  
    #if DEBUG
    /*
    for( int index = 0; index < CHAIN_LENGTH; index++ ){
      Serial.print("pixels[");
      Serial.print(index);
      Serial.print("].fades.size(): ");
      Serial.println( pixels[index].fades.size() );
    }
    */
    #endif
  
    //debug
    //Serial.print("connectedToWifi: ");
    //Serial.println(connectedToWifi);
    //delay(300);
    //
  
    #if USE_PING_TIME_TO_CHECK_DISCONNECTION
    if( (connectedToWifi && connectedToServer) ){
        //Serial.println("connectedToWifi && connectedToServer");
    
        long currentTime = millis();
        long timeDelta = currentTime - timeLastPingReceived;
        /*
          Serial.print("currentTime: ");
          Serial.println(currentTime);
          Serial.print("timeLastPing: ");
          Serial.println(timeLastPingReceived);
          Serial.print("timeDelta: ");
          Serial.println(timeDelta);
        */
    
        if( timeDelta > SERVER_TIMEOUT ){
            Serial.println("timeDelta > SERVER_TIMEOUT");
      
            /* this is how it used to be...
              #if DEBUG
              Serial.print("timeDelta > SERVER_TIMEOUT (");
              Serial.print(SERVER_TIMEOUT);
              Serial.println(")");
              #endif
              Serial.println(" ... settin own IP to 0.0.0.0");
              delay(1000);
              bool connectedToWifi = false;
              bool connectedToServer = false;
              turnOffAllPixels();
              ownIP = (0, 0, 0, 0);
              delay(1000);
            */
      
            // but let's try it in another way... because right now it's this situation: client is connecting to server, server is leaving the wifi, or just closing the socket, client checks this via SERVER_TIMEOUT and reconnects to wifi, which is not necessary...
            // so let's try it this way:
            #if DEBUG
            Serial.print("timeDelta > SERVER_TIMEOUT (");
            Serial.print(SERVER_TIMEOUT);
            Serial.println(")");
            #endif
            delay(1000);
            connectedToServer_old = connectedToServer;
            connectedToServer = false;
            if( connectedToServer_old == true && connectedToServer == false ){
                makeSignalForLostServerConnection();
                delay(1000);
            }
        }
    }
    #endif
  
    if( ownIP == (0, 0, 0, 0) ){  // IP: 0.0.0.0
        Serial.println( "IP = 0.0.0.0 ... setting connectedToWifi = false and connectedToWifi = false");
        connectedToServer = false;
        connectedToWifi = false;
        if( wifiIsOn ){
            Serial.println("wifiIsOn == true ... calling connectToWifi()");
            connectToWifi();
        }else{
            Serial.println("not calling connectToWifi() because wifiIsOn == false");
        }
    
        //delay(100);
    }else{
        //Serial.println("marking connectedToWifi to 'true'");
        if( connectedToWifi == false ){
            makeWifiConnectionSignal();
        }
        connectedToWifi = true;
        tryesToConnectToWifi = 0;
        //delay(100);
    }
  
    //Serial.print("client.status(): ");
    //Serial.println(client.status());
  
    if( connectedToServer == false && connectedToWifi == true ){  // well... this makes more sense, but this (next line).
        Serial.println("connectedToServer == false. trying to connect...");
        if( client.connect( SERVER_IP, 4223 )  ){
            //if( client.connect( "192.168.1.106", 4223 )  ){
            Serial.println("connected to server");
            connectedToServer = true;
            makeServerConnectedSignale();
            timeLastPingReceived = millis();
            writeServer( "@identify;" + String( ESP.getChipId() ) + "_" + __DATE__ + " " + __TIME__ + "#" );
            writeServer( "@state;up#");
            //writeServer( "@chainLength;" + String( chainLength ) + "#" );
        }else{
            tryesToConnectToServer++;
            Serial.print("couldn't connect to server ... tryesToConnectToServer: ");
            Serial.println( tryesToConnectToServer );
            if( tryesToConnectToServer == 10){
                Serial.println("tryesToConnectToServer == 10 reached");
                tryesToConnectToServer = 0;
                //Serial.print("getDoWifiForcedSleepInLoop(): ");
                //Serial.println(getDoWifiForcedSleepInLoop());
                if( getDoWifiForcedSleepInLoop() ){
                    Serial.println("getDoWifiForcedSleepInLoop() == true");
                    doWifiForcedSleepForSeconds( getWifiForcedSleepInLoopForSeconds() );
                }else if( getDoDeepSleepInLoop() ){
                    Serial.println("getDeepSleepInLoop() == true");
                    doDeepSleepForSeconds( getDeepSleepInLoopForSeconds() );
                }else{
                    Serial.println("... doing nothing ...");
                }
            }
            delay(500);
        }
    }else{
        #if DEBUG
        Serial.println("connectedToServer == true ");
        delay(500);
        #endif
    }
  
    if( connectedToServer && connectedToWifi ){
        #if DEBUG
        //Serial.println("connectedToServer && connectedToWifi");
        #endif
        readFromSocketAndExecute();
    
        long currentTime = millis();
        if( currentTime - timeLastPingSent > MIN_TIME_BETWEEN_PINGS ){
            updateSignalStrength(); // only updates the value after 10 calles...
            sendPingToServer();
      
            /*
              Serial.print("millis(): ");
              Serial.println(millis());
              Serial.print("Signal Strength: ");
              Serial.println( WiFi.RSSI() );
              Serial.print("millis(): ");
              Serial.println(millis());
            */
    
        }else{
          /*
            #if DEBUG
            Serial.print("to early to send next ping. currentTime - timeLastPingSent: ");
            Serial.println(currentTime - timeLastPingSent);
            #endif
          */
        }
    }else{
        Serial.println("(connectedToServer && connectedToWifi) == false ");
        delay(700);
    }
    
    updateAllPixels();
    checkButton0();
    checkTriggerIn();
}
